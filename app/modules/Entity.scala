/*
 * data-exporter
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package modules

import play.api.libs.json._

abstract class ExportField



// for persons, corporates, places...

case class SingleField(value: String) extends ExportField {
  override def toString: String = value
}

case class MultivaluedField(values: Iterable[String]) extends ExportField {
  override def toString: String = values.filter(_.nonEmpty).mkString("###")
}

case class Entity(name: Option[String],
                  date: Option[String],
                  identifier: Option[Seq[String]],
                  role: Option[Seq[String]],
                  description: Option[Seq[String]],
                  title: Option[String]
                 ) {

  override def toString: String = {
    //this is used in the csv export
    (name, title, identifier, role) match {
      case (Some(name), Some(title), Some(identifier), Some(role)) => s"$title / $name, ID: ${identifier.mkString(", ")}, Role: ${role.mkString(", ")}"
      case (Some(name), Some(title), Some(identifier), _) => s"$title / $name, ID: ${identifier.mkString(", ")}"
      case (Some(name), Some(title), _, Some(role)) => s"$title / $name, Role: ${role.mkString(", ")}"
      case (Some(name), Some(title), _, _) => s"$title / $name"
      case (Some(name), _, Some(identifier), Some(role)) => s"$name, ID: ${identifier.mkString(", ")}, Role: ${role.mkString(", ")}"
      case (Some(name), _, Some(identifier), _) => s"$name, ID: ${identifier.mkString(", ")}"
      case (Some(name), _, _, Some(role)) => s"$name, Role: ${role.mkString(", ")}"
      case (Some(name), _, _, _) => name
      case (_, Some(title), Some(identifier), Some(role)) => s"$title, ID: ${identifier.mkString(", ")}, Role: ${role.mkString(", ")}"
      case (_, Some(title), _, Some(role)) => s"$title, Role: ${role.mkString(", ")}"
      case (_, Some(title), _, _) => s"$title"
      case (_, _, _, _) => ""
    }
  }


}

case class Entities(entities: Iterable[Entity]) extends ExportField {
  override def toString: String = entities.filterNot(_.toString.isEmpty).mkString("###")
}

case class PageDownloadLink(label: Option[String],
                            page_id: Option[String],
                            order: Option[Int],
                            image_small: Option[String],
                            image_medium: Option[String],
                            image_large: Option[String],
                            fulltext_plain: Option[String],
                            fulltext_alto: Option[String],
                            transcription_markdown: Option[String],
                            transcription_html: Option[String]
                           )

case class PageDownloadLinks(pageDownloadLinks: Iterable[PageDownloadLink]) extends ExportField


