/*
 * data-exporter
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package modules

import play.api.libs.json.Json

import java.util
import scala.collection.immutable.Nil.mkString
import scala.collection.immutable.{ListMap, Map, Set}
import scala.jdk.CollectionConverters.CollectionHasAsScala
import scala.language.postfixOps

class FullExportMapper {

  // analog VuFind Übersetzungen
  val relator: Map[String, String] = Map(
    "abr" -> "KürzendeR",
    "acp" -> "HerstellerIn von Nachbildungen",
    "act" -> "SchauspielerIn",
    "adi" -> "Art Director",
    "adp" -> "BearbeiterIn",
    "aft" -> "VerfasserIn eines Nachworts",
    "anl" -> "AnalytikerIn",
    "anm" -> "TrickfilmzeichnerIn",
    "ann" -> "AnnotatorIn",
    "ant" -> "BibliographischeR VorgängerIn",
    "ape" -> "BerufungsbeklagteR/RevisionsbeklagteR",
    "apl" -> "BerufungsklägerIn/RevisionsklägerIn",
    "app" -> "AntragstellerIn",
    "aqt" -> "AutorIn von Zitaten oder Textabschnitten",
    "arc" -> "ArchitektIn",
    "ard" -> "künstlerische Leitung",
    "arr" -> "ArrangeurIn",
    "art" -> "KünstlerIn",
    "asg" -> "RechtsnachfolgerIn",
    "asn" -> "zugehöriger Name",
    "ato" -> "UnterzeichnerIn",
    "att" -> "zugehöriger Name",
    "auc" -> "AuktionatorIn",
    "aud" -> "AutorIn des Dialogs",
    "aui" -> "VerfasserIn eines Geleitwortes",
    "aus" -> "DrehbuchautorIn",
    "aut" -> "VerfasserIn",
    "bdd" -> "BindungsgestalterIn",
    "bjd" -> "EinbandgestalterIn",
    "bkd" -> "BuchgestalterIn",
    "bkp" -> "BuchherstellerIn",
    "blw" -> "AutorIn des Klappentextes",
    "bnd" -> "BuchbinderIn / Buchbinderei",
    "bnd" -> "BuchbinderIn",
    "bpd" -> "GestalterIn des Exlibris",
    "brd" -> "Sender",
    "brl" -> "BrailleschriftprägerIn",
    "bsl" -> "BuchhändlerIn",
    "cas" -> "FormgiesserIn",
    "ccp" -> "konzeptionelle Leitung",
    "chr" -> "ChoreografIn",
    "clb" -> "MitarbeiterIn",
    "cli" -> "KlientIn, AuftraggeberIn",
    "cll" -> "KalligrafIn",
    "clr" -> "KoloristIn",
    "clt" -> "LichtdruckerIn",
    "cmm" -> "KommentatorIn",
    "cmp" -> "KomponistIn",
    "cmt" -> "SchriftsetzerIn",
    "cnd" -> "DirigentIn",
    "cng" -> "Kameramann/frau",
    "cns" -> "ZensorIn",
    "coe" -> "BerufungsbeklagteR im streitigen Verfahren",
    "col" -> "SammlerIn",
    "com" -> "AktenbildnerIn",
    "con" -> "KonservatorIn",
    "cor" -> "AktenbildnerIn",
    "cos" -> "AnfechtendeR, bestreitende Partei",
    "cot" -> "BerufungsklägerIn im streitigen Verfahren",
    "cou" -> "zuständiges Gericht",
    "cov" -> "UmschlaggestalterIn",
    "cpc" -> "BeansprucherIn des Urheberrechts",
    "cpe" -> "BeschwerdeführerIn-BerufungsbeklagteR",
    "cph" -> "InhaberIn des Urheberrechts",
    "cpl" -> "BeschwerdeführerIn/KlägerIn",
    "cpt" -> "KlägerIn/BerufungsklägerIn",
    "cre" -> "GeistigeR SchöpferIn / AktenbildnerIn",
    "crp" -> "KorrespondentIn",
    "crr" -> "KorrektorIn",
    "crt" -> "GerichtsstenografIn",
    "csl" -> "BeraterIn",
    "csp" -> "ProjektberaterIn",
    "cst" -> "KostümbildnerIn",
    "ctb" -> "MitwirkendeR",
    "cte" -> "AnfechtungsgegnerIn-BerufungsbeklagteR",
    "ctg" -> "KartografIn",
    "ctr" -> "VertragspartnerIn",
    "cts" -> "AnfechtungsgegnerIn",
    "ctt" -> "AnfechtungsgegnerIn-BerufungsklägerIn",
    "cur" -> "KuratorIn",
    "cwt" -> "KommentatorIn",
    "dbp" -> "Vertriebsort",
    "dfd" -> "AngeklagteR/BeklagteR",
    "dfe" -> "AngeklagteR/BeklagteR-BerufungsbeklagteR",
    "dft" -> "AngeklagteR/BeklagteR-BerufungsklägerIn",
    "dgg" -> "Grad-verleihende Institution",
    "dgs" -> "AkademischeR BetreuerIn",
    "dir" -> "Dirigent",
    "dis" -> "PromovierendeR",
    "dln" -> "VorzeichnerIn",
    "dnc" -> "TänzerIn",
    "dnr" -> "DonatorIn",
    "dpc" -> "AbgebildeteR",
    "dpt" -> "LeihgeberIn",
    "drm" -> "TechnischeR ZeichnerIn",
    "drt" -> "RegisseurIn",
    "dsr" -> "DesignerIn",
    "dst" -> "Vertrieb",
    "dtc" -> "BereitstellerIn von Daten",
    "dte" -> "WidmungsempfängerIn",
    "dtm" -> "DatenmanagerIn",
    "dto" -> "WidmungsverfasserIn",
    "dub" -> "angeblicheR AutorIn",
    "edc" -> "BearbeiterIn der Zusammenstellung",
    "edm" -> "CutterIn",
    "edt" -> "HerausgeberIn",
    "egr" -> "StecherIn",
    "elg" -> "ElektrikerIn",
    "elt" -> "GalvanisiererIn",
    "eng" -> "IngenieurIn",
    "enj" -> "Normerlassende Gebietskörperschaft",
    "etr" -> "RadiererIn",
    "evp" -> "Veranstaltungsort",
    "exp" -> "ExperteIn",
    "fac" -> "SchreiberIn / Scriptorium",
    "fds" -> "Filmvertrieb",
    "fld" -> "BereichsleiterIn",
    "flm" -> "BearbeiterIn des Films",
    "fmd" -> "FilmregisseurIn",
    "fmk" -> "FilmemacherIn",
    "fmo" -> "VorbesitzerIn",
    "fmp" -> "FilmproduzentIn",
    "fnd" -> "GründerIn",
    "fpy" -> "Erste Partei",
    "frg" -> "FälscherIn",
    "gis" -> "GeographIn",
    "grt" -> "GraphischeR TechnikerIn",
    "hg" -> "Herausgeber",
    "his" -> "Gastgebende Institution",
    "hnr" -> "GefeierteR",
    "hst" -> "GastgeberIn",
    "ill" -> "IllustratorIn",
    "ilu" -> "IlluminatorIn",
    "ins" -> "Beschriftende Person",
    "inv" -> "ErfinderIn",
    "isb" -> "Herausgebendes Organ",
    "itr" -> "InstrumentalmusikerIn",
    "ive" -> "InterviewteR",
    "ivr" -> "InterviewerIn",
    "jud" -> "RichterIn",
    "jug" -> "zuständige Gerichtsbarkeit",
    "kad" -> "Kadenzverfasser",
    "lbr" -> "Labor",
    "lbt" -> "LibrettistIn",
    "ldr" -> "Laborleitung",
    "led" -> "Führung",
    "lee" -> "Libelee-appellee",
    "lel" -> "BeklagteR im Seerecht/Kirchenrecht",
    "len" -> "LeihgeberIn",
    "let" -> "Libelee-appellant",
    "lgd" -> "LichtgestalterIn",
    "lie" -> "Libelant-appellee",
    "lil" -> "KlägerIn im Seerecht/Kirchenrecht",
    "lit" -> "Libelant-appellant",
    "lsa" -> "LandschaftsarchitektIn",
    "lse" -> "LizenznehmerIn",
    "lso" -> "LizenzgeberIn",
    "ltg" -> "LithographIn",
    "lyr" -> "TextdichterIn",
    "mcp" -> "ArrangeurIn, Notenleser/-schreiberIn",
    "mdc" -> "Metadatenkontakt",
    "med" -> "Medium",
    "mfp" -> "Herstellungsort",
    "mfr" -> "HerstellerIn",
    "mod" -> "ModeratorIn",
    "mon" -> "BeobachterIn",
    "mrb" -> "MarmorarbeiterIn, MarmoriererIn",
    "mrk" -> "Markup-EditorIn",
    "msd" -> "MusikalischeR LeiterIn",
    "mte" -> "Metall-GraveurIn",
    "mtk" -> "ProtokollantIn",
    "mus" -> "MusikerIn",
    "nrt" -> "ErzählerIn",
    "opn" -> "GegnerIn",
    "org" -> "UrheberIn",
    "orm" -> "VeranstalterIn",
    "osp" -> "On-screen PräsentatorIn",
    "oth" -> "WeitereR MitwirkendeR",
    "own" -> "EigentümerIn",
    "pan" -> "DiskussionsteilnehmerIn",
    "pat" -> "AuftraggeberIn",
    "pbd" -> "Verlagsleitung",
    "pbl" -> "VerlegerIn / Verlag",
    "pdr" -> "Projektleitung",
    "pfr" -> "KorrektorIn",
    "pht" -> "FotografIn",
    "plt" -> "DruckformherstellerIn",
    "pma" -> "Genehmigungsstelle",
    "pmn" -> "Produktionsleitung",
    "pop" -> "PlattendruckerIn",
    "ppm" -> "PapierherstellerIn / Papiermühle",
    "ppt" -> "PuppenspielerIn",
    "pra" -> "Praeses",
    "prc" -> "Prozesskontakt",
    "prd" -> "Produktionspersonal",
    "pre" -> "PräsentatorIn",
    "prf" -> "DarstellerIn",
    "prg" -> "ProgrammiererIn",
    "prm" -> "DruckgrafikerIn",
    "prn" -> "Produktionsfirma",
    "pro" -> "ProduzentIn",
    "prp" -> "Produktionsort",
    "prs" -> "SzenenbildnerIn",
    "prt" -> "DruckerIn",
    "prv" -> "AnbieterIn",
    "pta" -> "PatentanwärterIn",
    "pte" -> "KlägerIn-BerufungsbeklagteR",
    "ptf" -> "ZivilklägerIn",
    "pth" -> "PatentinhaberIn",
    "ptt" -> "KlägerIn-BerufungsklägerIn",
    "pup" -> "Erscheinungsort",
    "rbr" -> "RubrikatorIn",
    "rcd" -> "TonmeisterIn",
    "rce" -> "ToningenieurIn",
    "rcp" -> "AdressatIn",
    "rdd" -> "HörfunkregisseurIn",
    "red" -> "RedakteurIn",
    "ren" -> "RendererIn (Bildverarbeitung)",
    "res" -> "ForscherIn",
    "rev" -> "RezensentIn, GutachterIn",
    "rpc" -> "HörfunkproduzentIn",
    "rps" -> "Aufbewahrungsort, TreuhänderIn",
    "rpt" -> "ReporterIn",
    "rpy" -> "Verantwortliche Partei",
    "rse" -> "AntragsgegnerIn-BerufungsbeklagteR",
    "rsg" -> "RegisseurIn der Wiederaufführung",
    "rsp" -> "Respondens",
    "rsr" -> "RestauratorIn",
    "rst" -> "AntragsgegnerIn-BerufungsklägerIn",
    "rth" -> "Leitung des Forschungsteams",
    "rtm" -> "Mitglied des Forschungsteams",
    "sad" -> "WissenschaftlicheR BeraterIn",
    "sce" -> "DrehbuchautorIn",
    "scl" -> "BildhauerIn",
    "scr" -> "SchreiberIn / Scriptorium",
    "sds" -> "Tongestalter",
    "sec" -> "SekretärIn",
    "sgd" -> "BühnenregisseurIn",
    "sgn" -> "UnterzeichnerIn",
    "sht" -> "Unterstützender Veranstalter",
    "sll" -> "VerkäuferIn",
    "sng" -> "SängerIn",
    "spk" -> "SprecherIn",
    "spn" -> "SponsorIn",
    "spy" -> "Zweite Partei",
    "srv" -> "LandvermesserIn",
    "std" -> "BühnenbildnerIn",
    "stg" -> "Kulisse",
    "stl" -> "GeschichtenerzählerIn",
    "stm" -> "InszenatorIn",
    "stn" -> "Normungsorganisation",
    "str" -> "StereotypeurIn",
    "tcd" -> "Technische Leitung",
    "tch" -> "AusbilderIn",
    "ths" -> "BetreuerIn (Doktorarbeit)",
    "tld" -> "FernsehregisseurIn",
    "tlp" -> "FernsehproduzentIn",
    "trc" -> "TranskribiererIn",
    "trl" -> "ÜbersetzerIn",
    "tyd" -> "Schrift-DesignerIn",
    "tyg" -> "SchriftsetzerIn",
    "uvp" -> "Hochschulort",
    "vac" -> "SynchronsprecherIn",
    "vdg" -> "BildregisseurIn",
    "voc" -> "VokalistIn",
    "wac" -> "KommentarverfasserIn",
    "wal" -> "VerfasserIn von zusätzlichen Lyrics",
    "wam" -> "AutorIn des Begleitmaterials",
    "wat" -> "VerfasserIn von Zusatztexten",
    "wdc" -> "HolzschneiderIn",
    "wde" -> "HolzschnitzerIn",
    "win" -> "VerfasserIn einer Einleitung",
    "wit" -> "ZeugeIn",
    "wpr" -> "VerfasserIn eines Vorworts",
    "wst" -> "VerfasserIn von ergänzendem Text"
  )

  // analog VuFind Übersetzungen
  val resourceType: Map[String, String] = Map(
      "fonds" -> "Archivmaterial / Bestand",
      "series" -> "Archivmaterial / Serie",
      "file" -> "Archivmaterial / Dossier",
      "item" -> "Archivmaterial / Archivdokument",
      "aquarell" -> "Aquarell",
      "aquatinta" -> "Aquatinta",
      "autotypie" -> "Autotypie",
      "bildnis" -> "Bildnis",
      "collage" -> "Collage",
      "cyanotypie" -> "Cyanotypie",
      "daguerreotypie" -> "Daguerreotypie",
      "digitalerAusdruck" -> "Digitaler Ausdruck",
      "druckplatte" -> "Druckplatte",
      "druckstock" -> "Druckstock",
      "fotografie" -> "Fotografie",
      "fotokopie" -> "Fotokopie",
      "gemaelde" -> "Gemälde",
      "glasmalerei" -> "Glasmalerei",
      "gouache" -> "Gouache",
      "grafik" -> "Grafik",
      "heliogravuere" -> "Heliogravüre",
      "holzschnitt" -> "Holzschnitt",
      "holzstich" -> "Holzstich",
      "kalender" -> "Kalender",
      "karikatur" -> "Karikatur",
      "kontaktkopie" -> "Kontaktkopie",
      "kupferstich" -> "Kupferstich",
      "laserprint" -> "Laserprint",
      "lichtdruck" -> "Lichtdruck",
      "linolschnitt" -> "Linolschnitt",
      "lithographie" -> "Lithographie",
      "modell" -> "Modell",
      "offsetdruck" -> "Offsetdruck",
      "pastellzeichnung" -> "Pastellzeichnung",
      "photochrom" -> "Photochrom",
      "plakat" -> "Plakat",
      "postkarte" -> "Postkarte",
      "praegedruck" -> "Prägedruck",
      "radierung" -> "Radierung",
      "rakeltiefdruck" -> "Rakeltiefdruck",
      "rasterdruck" -> "Rasterdruck",
      "schabkunst" -> "Schabkunst",
      "stahlstich" -> "Stahlstich",
      "strichklischee" -> "Strichklischee",
      "umrissradierung" -> "Umrissradierung",
      "zeichnung" -> "Zeichnung",
      "zinkdruck" -> "Zinkdruck",
      "work" -> "Gesamtwerk",
      "journal" -> "Zeitschrift",
      "newspaper" -> "Zeitung",
      "einblatt" -> "Einblattdruck",
      "person" -> "Personen Wirtschaft",
      "corporate" -> "Firmen und Organisationen",
      "subject" -> "Sachthemen Wirtschaft",
      "textms" -> "Buchhandschrift",
      "letter" -> "Brief",
      "autograph" -> "Autograph",
      "papyrus" -> "Papyrus",
      "fragment" -> "Fragment",
      "oldmap" -> "Altkarte",
      "atlas" -> "Atlas",
      "globus" -> "Globus",
      "map" -> "Karte",
      "mapwork" -> "Kartenwerk",
      "mapms" -> "Manuskriptkarte",
      "panorama" -> "Panorama",
      "plan" -> "Plan",
      "relief" -> "Relief (Geländemodell)",
      "profil" -> "Profil, Schnitt",
      "city" -> "Stadtplan",
      "wall" -> "Wandkarte",
      "world" -> "Weltkarte",
      "mapprint" -> "Gedruckte Karte",
      "cd" -> "CD / SACD",
      "record" -> "Schallplatte",
      "cassette" -> "DAT-Kassette / Tonband-Kassette",
      "reel" -> "Tonbandspule",
      "score" -> "Partitur",
      "study" -> "Studienpartitur",
      "reduction" -> "Klavierauszug",
      "piano" -> "Klavierbearbeitung",
      "condensed" -> "Particell",
      "chorus" -> "Chorpartitur",
      "parts" -> "Stimmen",
      "conductor" -> "Direktionsstimme",
      "performance" -> "Aufführungsmaterial",
      "musicms" -> "Musikhandschrift",
      "basel" -> "Eintrag Basler Bibliographie",
      "bern" -> "Eintrag Berner Bibliographie",
      "solothurn" -> "Eintrag Solothurner Bibliographie",
      "zurich" -> "Eintrag Zürcher Bibliographie",
      "AR" -> "Archivmaterial",
      "BE" -> "Bibliographischer Eintrag",
      "IO" -> "Bildmaterial",
      "PR" -> "Alte Drucke und Rara",
      "DS" -> "Dokumentensammlung",
      "VM" -> "Filmmaterial",
      "MS" -> "Handschrift",
      "TR" -> "Textaufnahme",
      "MP" -> "Karte",
      "MR" -> "Musikaufnahme",
      "SM" -> "Musiknoten",
      "XY" -> "Diverses"
  )

  val libraryName: Map[String, String] = Map(
    "A100" -> "Basel, Universitätsbibliothek",
    "A115" -> "Basler Bibliographie",
    "A116" -> "Basel, UB, Verzeichnis der Basler Drucke",
    "A117" -> "Bernoulli-Briefinventar",
    "A118" -> "Autographensammlung Geigy-Hagenbach",
    "A125" -> "Basel, Schweiz. Wirtschaftsarchiv",
    "A150" -> "Solothurn, Zentralbibliothek",
    "A380" -> "Beromünster, Stiftsbibliothek",
    "A381" -> "Frauenfeld, Kantonsbibliothek Thurgau",
    "A382" -> "Zofingen, Stadtbibliothek",
    "AKB" -> "Aarau, Aargauer Kantonsbibliothek",
    "B400" -> "Bern, UB Speichermagazin",
    "B404" -> "Bern, UB Münstergasse",
    "B410" -> "Bern, UB Unitobler",
    "B412" -> "Bibliographie der Berner Geschichte",
    "B415" -> "Bern, UB SOB",
    "B452" -> "Bern, UB Medizin",
    "B464" -> "Bern, UB JFB",
    "B465" -> "Bern, UB JBB",
    "B466" -> "Bern, UB Eugen Huber Bibliothek",
    "B467" -> "Bern, UB Geografie",
    "B500" -> "Bern, UB vonRoll",
    "B521" -> "Bern, UB Zahnmedizin",
    "B542" -> "Bern, Uni UPD WA",
    "B552" -> "Bern, UB Pflanzenwissenschaften",
    "B554" -> "Bern, UB Muesmatt",
    "B555" -> "Bern, UB Mittelstrasse",
    "B583" -> "Bern, UB Medizingeschichte",
    "B583RO" -> "Bern, IMG, Archiv Hermann Rorschach",
    "B589" -> "Bern, UB Wirtschaft",
    "E30" -> "Bern, UB Exakte Wissenschaften",
    "HAN001" -> "Uni Bern - Archiv und Sammlung Rorschach",
    "LUDOL" -> "Haus zum Dolder, Beromünster",
    "LUKIL" -> "ZHB Luzern - UNI/PH-Gebäude - RPI",
    "LULES" -> "ZHB Luzern - Sempacherstrasse",
    "LUNI3" -> "ZHB Luzern - UNI/PH-Gebäude - Fak. III",
    "LUPHL" -> "ZHB Luzern - UNI/PH-Gebäude - PHZ",
    "LUSBI" -> "Luzern, ZHB",
    "LUUHL" -> "ZHB Luzern - Uni/PH-Gebäude",
    "LUZHB" -> "Luzern, ZHB",
    "SGARK" -> "Trogen, Kantonsbibliothek Ausserrhoden",
    "SGKBN" -> "KB Vadiana St. Gallen - Naturhistorisches Museum",
    "SGKBV" -> "St. Gallen, Kantonsbibliothek Vadiana St. Gallen",
    "SGKHI" -> "KB Vadiana St. Gallen - Historisches Museum",
    "SGSTI" -> "St. Gallen, Stiftsbibliothek",
    "SGZFB" -> "KB Vadiana - St. Galler Zentrum für das Buch",
    "UMWI" -> "Zürich, ZB, Musikwissenschaftliches Institut der UZH",
    "Z01" -> "Zürich, ZB",
    "Z02" -> "Zürich, ZB, Graphische Sammlung",
    "Z03" -> "Zürich, ZB, Handschriftenabteilung",
    "Z04" -> "Zürich, ZB, Kartensammlung",
    "Z05" -> "Zürich, ZB, Musikabteilung",
    "Z06" -> "Zürich, ZB, Alte Drucke und Rara",
    "Z07" -> "Zürich, ZB, Bibliothek Schlag",
    "Z08" -> "Zürich, ZB, Verlagsbucharchiv",
    "UIPZ" -> "Zürich, ZB",
    "UDS" -> "Zürich, ZB",
    "UFBI" -> "Zürich, ZB",
    "UKHIS" -> "Zürich, ZB",
    "UHS" -> "Zürich, ZB",
  )

  // Hash with concordance MARC21 language codes and written language name
  val language: Map[String, String] = Map(
    "aar" -> "Afar",
    "afr" -> "Afrikaans",
    "alb" -> "Albanisch",
    "ang" -> "Altenglisch",
    "ara" -> "Arabisch",
    "arc" -> "Aramäisch",
    "arm" -> "Armenisch",
    "aze" -> "Azeri",
    "baq" -> "Baskisch",
    "bel" -> "Weissrussisch",
    "ben" -> "Bengali",
    "bos" -> "Bosnisch",
    "bul" -> "Bulgarisch",
    "bur" -> "Burmesisch",
    "chi" -> "Chinesisch",
    "chu" -> "Altbulgarisch, Kirchenslawisch",
    "cop" -> "Koptisch",
    "cze" -> "Tschechisch",
    "dan" -> "Dänisch",
    "dut" -> "Niederländisch",
    "egy" -> "Ägyptisch",
    "eng" -> "Englisch",
    "est" -> "Estnisch",
    "fin" -> "Finnisch",
    "fre" -> "Französisch",
    "geo" -> "Georgisch",
    "ger" -> "Deutsch",
    "gez" -> "Äthiopisch",
    "gla" -> "Gälisch",
    "gle" -> "Gälisch",
    "grc" -> "Altgriechisch",
    "gre" -> "Neugriechisch",
    "gsw" -> "Schweizerdeutsch",
    "heb" -> "Hebräisch",
    "hin" -> "Hindi",
    "hrv" -> "Kroatisch",
    "hun" -> "Ungarisch",
    "ice" -> "Isländisch",
    "ind" -> "Indonesisch",
    "ita" -> "Italienisch",
    "jav" -> "Javanisch",
    "jpn" -> "Japanisch",
    "kas" -> "Kashmiri",
    "kaz" -> "Kasachisch",
    "khm" -> "Khmer",
    "kir" -> "Kirisisch",
    "kor" -> "Koreanisch",
    "kur" -> "Kurdisch",
    "lat" -> "Lateinisch",
    "lav" -> "Lettisch",
    "lit" -> "Litauisch",
    "mac" -> "Mazedonisch",
    "may" -> "Malaiisch",
    "mon" -> "Mongolisch",
    "mul" -> "Mehrere Sprachen",
    "nor" -> "Norwegisch",
    "ota" -> "Osmanisch",
    "per" -> "Persisch",
    "pol" -> "Polnisch",
    "por" -> "Portugiesisch",
    "roh" -> "Rätoromanisch",
    "rom" -> "Romani",
    "rum" -> "Rumänisch",
    "rus" -> "Russisch",
    "san" -> "Sanskrit",
    "slo" -> "Slowakisch",
    "slv" -> "Slowenisch",
    "spa" -> "Spanisch",
    "srp" -> "Serbisch",
    "swa" -> "Swahili",
    "swe" -> "Schwedisch",
    "syr" -> "Syrisch",
    "tam" -> "Tamil",
    "tgk" -> "Tadschikisch",
    "tgl" -> "Philippinisch",
    "tha" -> "Siamesisch",
    "tuk" -> "Turkmenisch",
    "tur" -> "Türkisch",
    "ukr" -> "Ukrainisch",
    "urd" -> "Urdu",
    "uzb" -> "Usbekisch",
    "vie" -> "Vietnamisch",
    "wen" -> "Sorbisch",
    "yid" -> "Jiddisch"
  )

  def getCsvHeader: String = {
    columns.-("page_download_links").keys.mkString(";") + "\n"
  }

  def getId(record: VuFindSolrRecord): Option[SingleField] = {
    Some(SingleField(record.getControlfield("001")))
  }

  def getIdExcel(record: VuFindSolrRecord): Option[SingleField] = {
    val id = record.getControlfield("001")
    if(id != null) Some(SingleField(s"=\"$id\""))
    else None
  }

  def getPublicationStatement(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val publicationStatement = record.getFieldsAsMapInd("264", "0") ++
      record.getFieldsAsMapInd("264", "1") ++
      record.getFieldsAsMapInd("264", "2") ++
      record.getFieldsAsMapInd("264", "3")

    val value = {
      for (statement <- publicationStatement) yield {
        getFormattedField(statement, ListMap("a" -> "", "c" -> ", "))
      }
    }
    Some(MultivaluedField(Seq(value.mkString(" ; "))))
  }

  def getPhysicalDescription(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val physicalDescription = record.getFieldsAsMap("300")

    val value = {
      for (description <- physicalDescription) yield {
        val string = getFormattedField(description, ListMap("a" -> "", "b" -> " : ", "c" -> " ; ", "e" -> " + "))

        if (description.contains("3") && string.nonEmpty) string + " (" + description.getOrElse("3", "") + ")"
        else if (description.contains("3")) "(" + description.getOrElse("3", "") + ")"
        else string
      }
    }
    Some(MultivaluedField(Seq(value.mkString(" ; "))))
  }

  def getMedium(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val physicalDescription = record.getFieldsAsMap("340")

    val value = {
      for (description <- physicalDescription) yield {

        val string = getFormattedField(description, ListMap("a" -> "", "b" -> " : ", "c" -> " ; ", "e" -> " + "))

        if (description.contains("i") && string.nonEmpty) string + " (" + description.getOrElse("i", "") + ")"
        else if (description.contains("i")) "(" + description.getOrElse("i", "") + ")"
        else string
      }
    }
    val values = Seq(value.mkString(" ; ")).filter(_.nonEmpty)
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getLanguage(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val languageNote = record.getFieldsAsMap("546")
    val languageCodes = (Set(record.getControlfield("008").substring(35, 38).replace("   ", "und").replace("zxx", "und")) ++
      record.getSubfields("041", "a").getOrElse(Set()))
      .map(code => language.getOrElse(code, ""))

    val value = {
      for (note <- languageNote) yield {
        getFormattedField(note, ListMap("a" -> "", "b" -> ", "))
      }
    }

    val res = if (value.nonEmpty) value else languageCodes.filter(_.nonEmpty)
    Some(MultivaluedField(res))
  }

  def getInstitution(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val institutions = record.getSubfields("852", "b")
    institutions match {
      case Some(value) => Some(MultivaluedField(value))
      case None => None
    }
  }

  def getLinks(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val links = record.getFieldsAsMap("856", "4", " ") ++
      record.getFieldsAsMap("856", "4", "1") ++
      record.getFieldsAsMap("AVD")

    val values = {
      for (link <- links) yield {
        link.getOrElse("u", "")
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getLibraryCallNo(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val holdings = record.getFieldsAsMap("852")

    val values = {
      for (holding <- holdings) yield {
        libraryName(holding.getOrElse("b", "")) +
          (if (holding.contains("j")) ", " + holding.getOrElse("j", "") else "")
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values))

  }

  def getAddCallNumber(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val items = record.getFieldsAsMap("949")

    val values = {
      for (item <- items) yield {
        if (item.contains("h")) {
          libraryName(item.getOrElse("b", "")) +
            (if (item.contains("h")) ", " + item.getOrElse("h", "") else "")
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values))

  }

  def getDescriptionLevel(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val descriptionLevel = record.getFirstSubfield("351", "c")
    descriptionLevel match {
      case Some(value) => Some(MultivaluedField(Seq(value)))
      case None => None
    }
  }


  val licence: Map[String, String] = Map(
    "pdm" -> "Public Domain Mark",
  )
  //these are the column titles of the csv and the corresponding functions
  private val columns: ListMap[String, (VuFindSolrRecord) => Option[ExportField]] = ListMap(
    "library_call_number" -> getLibraryCallNo, // 852 $b (translated), $j
    "resource_type" -> getResourceType,// aus Index format_str_mv, übersetzt
    "digital_reproduction" -> getLinks, // 856 41 $u, 8564# $u, AVD $u
    "url" -> getUrl, // 856__ $u
    "title" -> getTitle, // 245 $a : $b
    "title_original_script" -> getTitleScript, // 245 $a : $b aus 880
    "statement_of_responsibility" -> getStatementOfResponsibility, // 245 $c
    "statment_of_responsibility_original_script" -> getStatementOfResponsibilityScript, // 245 $c aus 880
    "place_of_publication_or_production" -> getPlaceOfPublication, // 264 $a
    "place_of_publication_or_production_original_script" -> getPlaceOfPublicationScript, // 264 $a aus 880
    "publisher_or_producer" -> getPublisher, // 264 $b
    "publisher_or_producer_original_script" -> getPublisherScript, // 264 $b aus 880
    "date_of_publication_or_production" -> getDateOfPublication, // 264 $c +880
    "date_of_publication_or_production_original_script" -> getDateOfPublicationScript, // 264 $c aus 880
    "date_of_publication_or_production_standardised" -> getDateNormalized, // 008/046
    // "Entstehungsangaben" -> get, // 264 _0
    //"Erscheinungsangaben" -> get, // 264 _1
    //"Vertriebsangaben" -> get, // 264 _2
    //"Herstellungsangaben" -> get, // 264 _3
    "date_of_copyright" -> getDateOfCopyright, // 264 _4 $c
    "level_of_description" -> getDescriptionLevel, // 351 $c
    "edition_or_level_of_creation" -> getEdition, // 250 $a, $b
    "edition_or_level_of_creation_original_script" -> getEditionScript, // 250 $a, $b aus 880
    "map_data" -> getMapData, // 255 $a ; $b ; $c ; $d ; $e ; $f ; $g
    "physical_description" -> getPhysicalDescription, // 300 $a : $b ; $c + $e ($3)
    "physical_characteristics" -> getMedium, // 340 $a : $b ; $c + $e ($i)
    "series" -> getSeries, // 490 $a ; $v
    "series_original_script" -> getSeriesScript, // 490 $a ; $v aus 880
    "included_in" -> getIncludedIn, //773 $t. $g ($w)
    "person" -> getAuthorPerson, // 100, 700 (ohne Werke!)
    "corporate_body" -> getAuthorCorporate, // 110, 710 (ohne Werke!)
    "conference" -> getAuthorConference, // 111, 711 (ohne Werke!)
    "title_of_work" -> getWork, // 130, 730, 7xx Werke
    "uncontrolled_or_analytical_titles" -> getUncontrolledTitles, // 740
    "place_standardised" -> getPlace, // 751
    "alternative_titel" -> getAlternativeTitle, // 246 $a : $b
    "alternative_titel_original_script" -> getAlternativeTitleScript, // 246 $a : $b aus 880
    "former_title" -> getFormerTitle, // 247 $a : $b
    "preferred_or_citation_title" -> getCitationTitle, // 524 $a
    "uniform_title" -> getUniformTitle, // 240
    "subject_person" -> getSubjectPerson, // 600 (ohne Werke!)
    "subject_corporate_body" -> getSubjectCorporate, // 610 (ohne Werke!)
    "subject_occasion" -> getSubjectConference, // 611 (ohne Werke!)
    "subject_topic" -> getTopic, // 650
    "subject_geographical_entity" -> getSubjectPlace, // 651
    "subject_title_of_work" -> getSubjectWork, // 630, 6xx Werke!
    "subject_time" -> getTime, // 648 $a
    "subject_form" -> getForm, // 655 $a $$2 gnd-content, 655 $a $$2 gnd-carrier, 965 $a $$2uzb-ZK, 965 $a $$2uzb-ZG, 965 $a $$2uzb-ZH, 348 $a $$2 gnd-music
    "form_of_the_work" -> getFormWork, // 380
    "instrumentation" -> getInstrumentation, // 382 $2 idsmusi
    "subject_music" -> getSubjectMusic, // 655 $2idsmusg
    "contents_abstract" -> getAbstract, //520
    "contents" -> getContents, // 596 30, 596 31, 505
    "contents_original_script" -> getContentsScript, // 596 30, 596 31, 505 aus 880
    //"Inhalt" -> get, // 505
    "initia" -> getInitia, // 690 $2 han-A1
    "initia_of_prayers" -> getInitiaPrayers, // 690 $2 han-A2
    "structure" -> getStructure, //351 $a
    "accompanying_material" -> getAccompanyingMaterial, // 525
    "subject_cataloguing_basel_bibliography" -> getSubjectBasel, // 690 $2 ubs-AC
    "subject_cataloguing_bern_bibliography" -> getSubjectBern, // 690 $2 ube-BD, $2 ube-BE
    "general_note" -> getGeneralNote, // 500 $a
    "language" -> getLanguage, // 546 $a, 008 -> auf Deutsch übersetzt (wegen Freitext 546)
    "script_or_notation" -> getScriptNotation, // 546 $b
    "note_on_music" -> getNoteMusic, // 596 2_
    "original_edition" -> getOriginalEdition, // 534
    "note_on_reproduction" -> getNoteOnReproduction, // 533
    "film_contributors" -> getFilmContributors, // 508
    "performers" -> getPerformers, // 511
    "date_and_place_of_recording" -> getDatePlaceRecording, // 518
    "identification_of_item" -> getIdentificationItem, // 562
    "ismn" -> getIsmn, // 024 2_
    "fingerprint_identifier" -> getFingerprint, // 026
    "publisher_order_number" -> getPublisherNumber, // 028
    "isbn" -> getIsbn, // 020
    "bibliographical_reference" -> getBibliographicalReference, // 510
    "history_of_origin_possession_and_collection" -> getHistory, // 561
    "acquisitions" -> getAcquisitions, // 584
    "accession" -> getAccession, // 541 $a, $c, $d, $e, $f, $3 with prefix per subfield, same as EAD-Export
    "history_of_creator_or_compiler" -> getHistoryCreator, // 545
    "exhibitions" -> getExhibitions, // 585
    "former_call_number" -> getFormerCallNumber, // 690 $a, $e if $2 han-A5 (case insensitive)
    "additional_call_numbers" -> getAddCallNumber, // 949 $b (translated), $h
    "notes_on_the_item" -> getNotesItem, // 590
    "binding" -> getBinding, // 563
    "support" -> getSupport, //596 0_ $3 : $a
    "sections" -> getSections, // 596 0_ $3 : $b
    "foliations" -> getFoliations, // 596 0_ $3 : $c
    "rubrications" -> getRubrications, // 596 1_ $3 : $a
    "initials" -> getInitials, // 596 1_ $3 : $b
    "miniatures_or_drawings" -> getMiniatures, // 596 1_ $3 : $c
    "iconography" -> getIconography, // 690 $2han-A3
    "page_layout" -> getPageLayout, // 596 1_ $3 : $d
    "writing" -> getWriting, // 596 1_ $3 : $e
    "additions" -> getAdditions, // 596 1_ $3 : $f
    "literature" -> getLiterature, // 581
    "finding_aids" -> getFindingAids, // 690 $2 han-A4
    "external_link" -> getExternalLink, // 856 42 $u
    "related_material" -> getRelatedMaterial, // 544
    "additional_forms_of_publication" -> getAdditionalForms, // 530
    "classification_basel_bibliography" -> getClassificationBasel, // 691 $e $2 ubs-FA
    "classification_bern_bibliography" -> getClassificationBern, //691 $e $$2 ube-GA
    "classification_zurich_bibliography" -> getClassificationZurich, // 990 $a /^ZueBiSACH.+/i"
    "classification_solothurn_bibliography" -> getClassificationSolothurn, // 691 $e $2 zbs-bib
    "bibliographic_code" -> getBibliographicCode, // 990 $b basbXX, "990 $b bbgXX", 990 "$a~/^ZueBiJAHR.+/i", "990 $a sobib"
    "terms_of_access" -> getTermsAccess, // 506
    "term_of_protection" -> getTermProtection, // 355 0_
    "references_to_finding_aids" -> getReferencesFindingAids, // 555
    "legal_provisions" -> getLegalProvisions, // 540
    "object_copyright" -> getObjectCopyright, // 542 1_
    "metadata_copyright" -> getMetadataCopyright, // 910, prüfen ob als URL
    // "digital_reproduction_citation" -> getReproductionCitation, // e-* Quellenangabe erstellen
    "digital_reproduction_copyright" -> getReproductionCopyright, // Lizenz von e-* als URL?
    "digital_reproduction_rights_owner" -> getReproductionRightsOwner, // owner von e-*
    "internal_processing" -> getInternalProcessing, // 583 1_
    "identifier_swisscollections" -> getId, // 001
    "identifier_swisscollections_for_excel" -> getIdExcel,
    "different_system_control_number" -> getDifferentSystemControlNumber, //035 $a
    "digital_object_identifier" -> getDoi, // 024 7_ $2doi
    "digital_platform" -> getPlatform, // platform
    "id_digital_platform" -> getPlatformId, // vlid
    "iiif_manifest_url" -> getManifest, // iiif
    "pdf_download_link" -> getPdfDownloadLink, // pdf link -> für url.csv und für json!
    "thumbnail" -> getThumbnail, // thumbnail URL
    "page_download_links" -> getPageDownloadLinks
  )

  def mapToCsv(exportRecord: ExportRecord): String = {
    exportRecord.record.map{
      case ("page_download_links", field) => "" //we skip the page_download_links in csv
      case (key, field) => field.getOrElse("").toString.replaceAll("\"", "\"\"") //escape " in csv with ""
    }.mkString("\"", "\";\"", "\"") //create csv line
  }


  def convertToExportRecord(record: VuFindSolrRecord): ExportRecord = {
    //println(getId(record))
    val resultList: ListMap[String, Option[ExportField]] = {
      for ((key, columnFunction) <- columns) yield {
        val res = columnFunction(record)
        key ->res
      }
    }.to(ListMap)
    ExportRecord(resultList)
  }

  def mapToJs(exportRecord: ExportRecord): String = {
    //todo compute resultList only once !!!

    val nonEmptyFields = exportRecord.record.collect {
      case (key, Some(value)) if value.toString.nonEmpty && key != "identifier_swisscollections_for_excel" => key -> value
    }

    implicit val singleFieldWrites = Json.writes[SingleField]
    implicit val multivaluedFieldWrites = Json.writes[MultivaluedField]
    implicit val entityWrites = Json.writes[Entity]
    implicit val entitiesWrites = Json.writes[Entities]
    implicit val pageDownloadLinkWrites = Json.writes[PageDownloadLink]
    implicit val pageDownloadLinksWrites = Json.writes[PageDownloadLinks]

    nonEmptyFields.map {
      case (key, singleValue: SingleField) => s"  \"$key\" : ${Json.toJson(singleValue.value)}"
      case (key, multiValues: MultivaluedField) => s"  \"$key\" : ${Json.toJson(multiValues.values)}"
      case (key, entities: Entities) => s"  \"$key\" : ${Json.toJson(entities.entities)}"
      case (key, pageDownloadLinks: PageDownloadLinks) =>  s"  \"$key\" : ${Json.toJson(pageDownloadLinks.pageDownloadLinks)}"
    }.mkString("{\n", ",\n", "\n}") //create js line
  }


  def generateUrlCsv(exportRecord: ExportRecord): String = {
    val links = exportRecord.record("page_download_links")
    val id: String = exportRecord.record("identifier_swisscollections").get.toString
    val res: String = links match {
      case Some(pageDownloadLinks: PageDownloadLinks) => pageDownloadLinks.pageDownloadLinks.map(doc => generatePageLinkCsv(id, doc)).mkString("", "\n","\n")
      case _ => ""
    }
    res
  }

  def generatePageLinkCsv(recordId: String, pageDownloadLink: PageDownloadLink): String = {
    //Identifier swisscollections;label;page_id;order;image_small;image_medium;image_large;fulltext_plain;fulltext_alto;transcription_markdown;transcription_html
      val row = List(
        recordId,
        pageDownloadLink.label.getOrElse("").replaceAll("\"", "\"\"").mkString("\"", "","\""),
        pageDownloadLink.page_id.getOrElse(""),
        pageDownloadLink.order.getOrElse(""),
        pageDownloadLink.image_small.getOrElse(""),
        pageDownloadLink.image_medium.getOrElse(""),
        pageDownloadLink.image_large.getOrElse(""),
        pageDownloadLink.fulltext_plain.getOrElse(""),
        pageDownloadLink.fulltext_alto.getOrElse(""),
        pageDownloadLink.transcription_markdown.getOrElse(""),
        pageDownloadLink.transcription_html.getOrElse("")
      )
    row.mkString(";") //create csv line
  }


  def getParentResourceType(record: VuFindSolrRecord): Option[MultivaluedField] = {
    Option(record.getFirstValue("format_parent_str_mv")) match {
      case Some(value) => Some(MultivaluedField(Seq(value.toString)))
      case None => None
    }
  }

  def getResourceType(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val resourceTypes = record.getFieldValues("format_str_mv").asInstanceOf[util.ArrayList[String]].asScala

    //todo improve this
    if (resourceTypes == null) {
      None
    } else {
      val mappedResourceTypes = resourceTypes.map(value => resourceType.getOrElse(value, value))
      Option.when(mappedResourceTypes.nonEmpty)(MultivaluedField(mappedResourceTypes))
    }
  }

  def getReproductionCopyright(record: VuFindSolrRecord): Option[SingleField] = {
    Option(record.getFirstValue(("digital_reproduction_licence_str_mv"))) match {
      case Some(value) => Some(SingleField(value.toString))
      case None => None
    }
  }

  def getReproductionRightsOwner(record: VuFindSolrRecord): Option[SingleField] = {
    Option(record.getFirstValue(("digital_reproduction_right_owner_str_mv"))) match {
      case Some(value) => Some(SingleField(value.toString))
      case None => None
    }
  }

  def getPlatform(record: VuFindSolrRecord): Option[SingleField] = {
    Option(record.getFirstValue(("digital_platform_str_mv"))) match {
      case Some(value) => Some(SingleField(value.toString))
      case None => None
    }
  }

  def getPlatformId(record: VuFindSolrRecord): Option[SingleField] = {
    Option(record.getFirstValue(("id_digital_platform_str_mv"))) match {
      case Some(value) => Some(SingleField(value.toString))
      case None => None
    }
  }

  def getManifest(record: VuFindSolrRecord): Option[SingleField] = {
    Option(record.getFirstValue("iiif_manifest_url_str_mv")) match {
      case Some(value) => Some(SingleField(value.toString))
      case None => None
    }
  }

  def getPdfDownloadLink(record: VuFindSolrRecord): Option[SingleField] = {
    val platform = getPlatform(record)
    val vlid = getPlatformId(record)
    (platform, vlid) match {
      case (Some(platform), Some(vlid)) => Some(SingleField(s"https://www.$platform/download/pdf/$vlid"))
      case (_, _) => None
    }
  }

  def getPageDownloadLinks(record: VuFindSolrRecord): Option[PageDownloadLinks] = {
    val pageDownloadLinks = record.getFieldValues("digital_object_pages_details_str_mv").asInstanceOf[util.ArrayList[String]].asScala
    val platform = getPlatform(record).getOrElse("")
    if (pageDownloadLinks == null || pageDownloadLinks.isEmpty) {
      None
    } else {
      val results = {
        for (page <- pageDownloadLinks) yield {
          val page_fields = page.split("###")
          val page_id = Option.when(page_fields(0).nonEmpty)(page_fields(0))
          val label = Option.when(page_fields(1).nonEmpty)(page_fields(1))
          val order = Option.when(page_fields(2).nonEmpty)(page_fields(2).toInt)
          val fulltext_available: Option[Boolean] = Option.when(page_fields(3).nonEmpty)(page_fields(3)=="1")

          val image_small: Option[String] = page_id match {
            case Some(page_id) => Some(s"https://www.$platform/download/webcache/128/$page_id")
            case _ => None
          }

          val image_medium: Option[String] = page_id match {
            case Some(page_id) => Some(s"https://www.$platform/download/webcache/504/$page_id")
            case _ => None
          }

          val image_large: Option[String] = page_id match {
            case Some(page_id) => Some(s"https://www.$platform/download/webcache/0/$page_id")
            case _ => None
          }

          //For e-rara : fulltexts plain and alto
          val fulltext_plain: Option[String] = (platform.toString, page_id, fulltext_available) match {
            case ("e-rara.ch", Some(page_id), Some(true)) => Some(s"https://www.$platform/download/fulltext/plain/$page_id")
            case (_, _, _) => None
          }

          val fulltext_alto: Option[String] = (platform.toString, page_id, fulltext_available) match {
            case ("e-rara.ch", Some(page_id), Some(true)) => Some(s"https://www.$platform/download/fulltext/alto3/$page_id")
            case (_, _, _) => None
          }

          //For e-manuscripta : transcriptions markdown and html
          val transcription_markdown: Option[String] = (platform.toString, page_id, fulltext_available) match {
            case ("e-manuscripta.ch", Some(page_id), Some(true)) => Some(s"https://www.$platform/download/fulltext/raw/$page_id")
            case (_, _, _) => None
          }

          val transcription_html: Option[String] = (platform.toString, page_id, fulltext_available) match {
            case ("e-manuscripta.ch", Some(page_id), Some(true)) => Some(s"https://www.$platform/download/fulltext/html/$page_id")
            case (_, _, _) => None
          }

          PageDownloadLink(page_id = page_id, label = label, order = order, image_small = image_small, image_medium = image_medium, image_large = image_large, fulltext_plain = fulltext_plain, fulltext_alto = fulltext_alto, transcription_markdown = transcription_markdown, transcription_html = transcription_html)
        }
      }
      Some(PageDownloadLinks(results))
    }
  }

  def getReproductionCitation(record: VuFindSolrRecord): Option[SingleField] = {
    //todo use the above method to get the data
    val rights = Option(record.getFirstValue("digital_reproduction_licence_str_mv")).getOrElse("").toString

    if (rights.nonEmpty) {
      val rightsOwner = Option(record.getFirstValue("digital_reproduction_right_owner_str_mv")).getOrElse("").toString
      val callNumber = Option(record.getFirstValue("digital_reproduction_call_number_str_mv")).getOrElse("").toString
      val doi = getSingleSubfieldSource(record, "024", "a", "doi")
      val title = getTitle(record).getOrElse("").toString // 245 $a : $b
      val publicationStatement = getPublicationStatementFull(record).getOrElse("").toString // 264 $a : $b , $c
      val authorMain = record.getFirstSubfield("100", "a").getOrElse("") // 100 $a
      val author = getAuthorsString(record, "700", "a", "aut") // alle 700 $a mit $4 aut

      val citationBuilder = new StringBuilder

      if (authorMain.nonEmpty) {
        citationBuilder.append(authorMain).append(": ")
      }
      else if (author.nonEmpty) {
        citationBuilder.append(author).append(": ")
      }

      if (title.nonEmpty) {
        citationBuilder.append(title)
      }

      if (publicationStatement.nonEmpty) {
        citationBuilder.append(". ").append(publicationStatement)
      }

      if (rightsOwner.nonEmpty) {
        citationBuilder.append(". ").append(rightsOwner)
      }

      if (callNumber.nonEmpty) {
        citationBuilder.append(", ").append(callNumber)
      }

      if (doi.nonEmpty) {
        citationBuilder.append(" https://doi.org/").append(doi.head)
      }

      if (rights.nonEmpty) {
        citationBuilder.append(" / ").append(licence.getOrElse(rights, ""))
      }

      Some(SingleField(citationBuilder.toString()))
    }
    else None
  }

  def getTitle(record: VuFindSolrRecord): Option[SingleField] = {
    val title = record.getFirstSubfield("245", "a").getOrElse("").replace("<", "").replace(">", "")
    val subtitle = record.getFirstSubfield("245", "b").getOrElse("")

    if (subtitle.nonEmpty) {
      Some(SingleField(s"$title : $subtitle"))
    } else {
      Some(SingleField(title))
    }
  }

  def getTitleAndResponsibility(record: VuFindSolrRecord): Option[SingleField] = {
    val title = record.getFirstSubfield("245", "a").getOrElse("").replace("<", "").replace(">","")
    val statementOfResponsibility = record.getFirstSubfield("245", "c").getOrElse("")
    if (statementOfResponsibility.nonEmpty) {
      Some(SingleField(s"$title / $statementOfResponsibility"))
    } else {
      Some(SingleField(title))
    }
  }

  def getTitleScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("245-")) {
          getFormattedField(field, ListMap("a" -> "", "b" -> " : "))
        }
        else ""
      }
    }
    values.filter(_.nonEmpty) match {
      case values if values.isEmpty => None
      case values => Some(MultivaluedField(values))
    }
  }

  def getAlternativeTitle(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("246")

    val values = {
      for (field <- fields) yield {
        getFormattedField(field, ListMap("a" -> "", "b" -> " : "))
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAlternativeTitleScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("246-")) {
          getFormattedField(field, ListMap("a" -> "", "b" -> " : "))
        }
        else ""
      }
    }

    values.filter(_.nonEmpty) match {
      case values if values.isEmpty => None
      case values => Some(MultivaluedField(values))
    }
  }

  def getFormerTitle(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("247")

    val values = {
      for (field <- fields) yield {
        getFormattedField(field, ListMap("a" -> "", "b" -> " : "))
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getIsbn(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("020", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getIsmn(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("024", "2")
    val values = {
      for (field <- fields) yield {
        field.getOrElse("a", "")
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getDoi(record: VuFindSolrRecord): Option[MultivaluedField] = {
    getSingleSubfieldSource(record, "024", "a", "doi") match {
      case values if values.isEmpty => None
      case values => Some(MultivaluedField(values))
    }
  }

  def getFingerprint(record: VuFindSolrRecord): Option[MultivaluedField] = {
    getAllSubfields(record, "026") match {
      case values if values.isEmpty => None
      case values => Some(MultivaluedField(values))
    }
  }

  def getPublisherNumber(record: VuFindSolrRecord): Option[MultivaluedField] = {
    getAllSubfields(record, "028") match {
      case values if values.isEmpty => None
      case values => Some(MultivaluedField(values))
    }
  }

  def getDifferentSystemControlNumber(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("035", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getUniformTitle(record: VuFindSolrRecord): Option[SingleField] = {
    val titles = record.getFieldsAsMap("240")

    val value = {
      if (titles.nonEmpty) getFormattedField(titles.head, ListMap(
        "a" -> "",
        "g" -> ". ",
        "m" -> ", ",
        "n" -> ". ",
        "r" -> ", ",
        "f" -> ". ",
        "s" -> ". ",
        "p" -> ", ",
        "k" -> ". ",
        "o" -> " ; ",
      ))
    }

    Option.when(titles.nonEmpty)(SingleField(value.toString))

  }

  def getStatementOfResponsibility(record: VuFindSolrRecord): Option[SingleField] = {
    getSingleSubfield(record, "245", "c") match {
      case "" => None
      case value => Some(SingleField(value))
    }
  }

  def getStatementOfResponsibilityScript(record: VuFindSolrRecord): Option[SingleField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("245-")) {
          field.getOrElse("c", "")
        }
        else ""
      }
    }
    Option.when(values.nonEmpty)(SingleField(values.filter(_.nonEmpty).mkString("|")))

  }

  def getEdition(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("250")

    val values = {
      for (field <- fields) yield {
          getFormattedField(field, ListMap("a" -> "", "b" -> ", "))
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getEditionScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("250-")) {
          getFormattedField(field, ListMap("a" -> "", "b" -> ", "))
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getMapData(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("255")

    val values = {
      for (field <- fields) yield {
        getFormattedField(field, ListMap("a" -> "", "b" -> "; ", "c" -> "; ", "d" -> "; ", "e" -> "; ", "f" -> "; ", "g" -> "; "))
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getPlaceOfPublication(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("264", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getPlaceOfPublicationScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("264-")) {
          field.getOrElse("a", "")
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getPublisher(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("264", "b") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getPublisherScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("264-")) {
          field.getOrElse("b", "")
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getDateOfPublication(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("264", "c") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getDateOfPublicationScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("264-")) {
          field.getOrElse("c", "")
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))

  }

  def getDateOfCopyright(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val dates = record.getSubfields("264", "a", "4")

    dates match {
      case Some(value) => Some(MultivaluedField(value))
      case None => None
    }
  }

  def getPublicationStatementFull(record: VuFindSolrRecord): Option[SingleField] = {
    val fields = record.getFieldsAsMap("264")

    val value = {
      for (field <- fields) yield {
        getFormattedField(field, ListMap("a" -> "", "b" -> " : ", "c" -> ", "))
      }
    }

    Option.when(value.nonEmpty)(SingleField(value.head))
  }

  def getStructure(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("351", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getTermProtection(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "355", "0")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getFormWork(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "380")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getInstrumentation(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val lists = record.getFieldsAsList("382")

    val values = {
      for (list <- lists) yield {
        val processedSubfields = processSubfieldsAN(list)
        processedSubfields.map(_._2).mkString(", ")
      }
    }

   Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
}

  def processSubfieldsAN(subfields: List[(String, String)]): List[(String, String)] = subfields match {
    case Nil => Nil
    case List((code, text)) if code == "a" => List((code, text))  // if single remaining element is 'a', keep it
    case List(_) => Nil  // discard any single remaining element that's not 'a'
    case (code1, text1) :: (code2, text2) :: rest if code1 == "a" && code2 == "n" =>
      processSubfieldsAN(("a", text1 + " (" + text2 + ")") :: rest)
    case (code1, text1) :: rest if code1 == "a" =>
      (code1, text1) :: processSubfieldsAN(rest) // keep 'a' subfields separately if not followed by 'n'
    case (_ :: rest) => processSubfieldsAN(rest)
  }

  def getSeries(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("490")

    val values = {
      for (field <- fields) yield {
        getFormattedField(field, ListMap("a" -> "", "v" -> "; "))
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getSeriesScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")

    val values = {
      for (field <- fields) yield {
        if (field.contains("6") && field.getOrElse("6", "").startsWith("490-")) {
          getFormattedField(field, ListMap("a" -> "", "v" -> "; "))
        }
        else ""
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }


  def getGeneralNote(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("500", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case none => None
    }
  }

  def getTermsAccess(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "506")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getFilmContributors(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "508")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getBibliographicalReference(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "510")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getPerformers(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "511")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getDatePlaceRecording(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "518")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAbstract(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "520")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getCitationTitle(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("524", "a") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getAccompanyingMaterial(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "525")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAdditionalForms(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "530")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getNoteOnReproduction(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "533")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getOriginalEdition(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "534")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getLegalProvisions(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "540")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAccession(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("541")

    val values = {
      for (acqnote <- fields) yield {
        ((if (acqnote.contains("3")) acqnote.getOrElse("3", "") + ": " else "") +
          (if (acqnote.contains("c")) acqnote.getOrElse("c", "") + ". " else "") +
          (if (acqnote.contains("a")) "Herkunft: " + acqnote.getOrElse("a", "") + ". " else "") +
          (if (acqnote.contains("d")) "Datum: " + acqnote.getOrElse("d", "") + ". " else "") +
          (if (acqnote.contains("e")) "Akzessionsnummer: " + acqnote.getOrElse("e", "") + ". " else "") +
          (if (acqnote.contains("f")) "Eigentümer: " + acqnote.getOrElse("f", "") else "")).trim
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getObjectCopyright(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "542", "1")
    Option.when(values.nonEmpty)(MultivaluedField(values))

  }

  def getRelatedMaterial(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "544")
    Option.when(values.nonEmpty)(MultivaluedField(values))

  }

  def getHistoryCreator(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "545")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getScriptNotation(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("546", "b") match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getReferencesFindingAids(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "555")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getHistory(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "561")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getIdentificationItem(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "562")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getBinding(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "563")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getLiterature(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "581")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getInternalProcessing(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "583", "1")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAcquisitions(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "584")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getExhibitions(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "585")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getNotesItem(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "590")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getNoteMusic(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "596", "2")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getSupport(record:VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "0", "a")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getSections(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "0", "b")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getFoliations(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "0", "c")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getRubrications(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "a")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getInitials(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "b")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getMiniatures(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "c")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getPageLayout(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "d")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getWriting(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "e")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAdditions(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getHanNote(record, "1", "f")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getClassificationBasel(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record, "691", "e", "ubs-FA")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getClassificationBern(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record, "691", "e", "ube-GA")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getClassificationSolothurn(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record, "691", "e", "sobib")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getClassificationZurich(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("990")

    val values = {
      for (field <- fields) yield {
        if (field.contains("a") && field.getOrElse("a", "").startsWith("ZueBiSACH")) {
          field.getOrElse("a", "")
        }
        else ""
      }
    }.filter(_.nonEmpty)

    Option.when(values.nonEmpty)(MultivaluedField(values))

  }

  def getBibliographicCode(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("990")

    val values = {
      for (field <- fields) yield {
        if (field.contains("a") && field.getOrElse("a", "").startsWith("ZueBiJAHR")) {
          field.getOrElse("a", "")
        }
        else if (field.contains("a") && field.getOrElse("a", "").startsWith("sobib")) {
          field.getOrElse("a", "")
        }
        else if (field.contains("b") && field.getOrElse("b", "").startsWith("basb")) {
          field.getOrElse("b", "")
        }
        else if (field.contains("b") && field.getOrElse("b", "").startsWith("bbg")) {
          field.getOrElse("b", "")
        }
        else ""
      }
    }.filter(_.nonEmpty)

    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getInitia(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record,"690", "a", "han-A1")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getInitiaPrayers(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record,"690", "a", "han-A2")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getIconography(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record,"690", "a", "han-A3")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getFindingAids(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record,"690", "a", "han-A4")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getFormerCallNumber(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("690")

    val values = {
      for (field <- fields) yield {
        if (field.contains("2") && field.getOrElse("2", "") == "han-A5") {
          getFormattedField(field, ListMap("a" -> "", "e" -> ", "))
        } else {
          ""
        }
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getSubjectBasel(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getSingleSubfieldSource(record, "690", "a", "ubs-AC")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getSubjectBern(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val ubeBD = getSingleSubfieldSource(record, "690", "a", "ube-BD")
    val ubeBE = getSingleSubfieldSource(record, "690", "a", "ube-BE")

    val values = ubeBD ++ ubeBE
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getUncontrolledTitles(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "740")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getIncludedIn(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getHostItemAsMap("773")

    val values = {
      for (field <- fields) yield {
        (if (field.contains("t")) field.getOrElse("t", "") else "") +
        (if (field.contains("w")) " (SLSP ID: " + field.getOrElse("w", "") + ")" else "") +
        (if (field.contains("g")) ". " + field.getOrElse("g", "") else "")
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getUrl(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val urls = record.getSubfields("856", "u", " ", " ")

    urls match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getExternalLink(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val urls = record.getSubfields("856", "u", "4", "2")

    urls match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }

  def getMetadataCopyright(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val values = getAllSubfields(record, "910")
    Option.when(values.nonEmpty)(MultivaluedField(values))
  }

  def getAuthorPerson(record: VuFindSolrRecord): Option[Entities] = {
    val persons = record.getFieldsAsMap("100") ++ record.getFieldsAsMap("700")
    val otherNormFiles = record.getFieldsAsMap("880")

    val entities = for {
      entity <- persons
      if !entity.contains("t")
    } yield {
          val name = getPersonName(entity)
          val date = entity.getOrElse("d", None) match {
            case None => None
            case value => Some(value.toString)
          }
          val identifiers = getIdentifiers(entity, otherNormFiles)
          val roles = getRoles(entity)
          Entity(
            name=name,
            date=date,
            identifier=identifiers,
            role=roles,
            description = None,
            title=None
          )
      }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getPersonName(field: Map[String, String]): Option[String] = {
      val formattedField = getFormattedField(field, ListMap(
        "a" -> "",
        "q" -> " (",
        "b" -> " ",
        "c" -> ", ",
      ))
      Some(formattedField)
  }

  def getCorporateName(field: Map[String, String]): Option[String] = {
      val formattedField = getFormattedField(field, ListMap(
        "a" -> "",
        "b" -> ". ",
      ))
      Some(formattedField)
  }

  def getConferenceName(field: Map[String, String]): Option[String] = {
    val formattedField = getFormattedField(field, ListMap(
      "a" -> "",
      "e" -> ". ",
    ))
    Some(formattedField)
  }

  def getWorkTitle(field: Map[String, String]): Option[String] = {
    val formattedField = getFormattedField(field, ListMap(
      "a" -> "",
      "g" -> ". ",
      "m" -> ", ",
      "n" -> ". ",
      "r" -> ", ",
      "f" -> ". ",
      "s" -> ". ",
      "p" -> ", ",
      "k" -> ". ",
      "o" -> " ; ",
    ))
    Some(formattedField)
  }

  def getWorkTitleT(field: Map[String, String]): Option[String] = {
    val formattedField = getFormattedField(field, ListMap(
      "t" -> "",
      "g" -> ". ",
      "m" -> ", ",
      "n" -> ". ",
      "r" -> ", ",
      "f" -> ". ",
      "s" -> ". ",
      "p" -> ", ",
      "k" -> ". ",
      "o" -> " ; ",
    ))
    Some(formattedField)
  }

  def getPersonWorkName(field: Map[String, String]): Option[String] = {
      val formattedField = getFormattedField(field, ListMap(
        "a" -> "",
        "q" -> " (",
        "b" -> " ",
        "c" -> ", ",
        "d" -> " ("
      ))
      Some(formattedField)
  }

  def getIdentifiers(entity: Map[String, String], otherNormFiles: Seq[Map[String, String]] = Seq.empty): Option[Seq[String]] = {

    val identifier = entity.get("0").flatMap(Some(_))
    // Get the corresponding linked entry in 880 based on subfield 6
    val linkedEntry = otherNormFiles.find(otherNormFile =>
      otherNormFile.getOrElse("6", "not-found") //todo improve this
        .substring(4, 6)
        .contains(entity
          .getOrElse("6", "not-found")
          .substring(4, 6)
        )
    )



    // Get the identifier from the linked entry
    val linkedIdentifier = linkedEntry.flatMap(_.get("0").flatMap(Some(_)))
    (identifier, linkedIdentifier) match {
      case (Some(id1), Some(id2)) => Some(Seq(id1, id2))
      case (Some(id1), None) => Some(Seq(id1))
      case (None, Some(id2)) => Some(Seq(id2))
      case _ => None
    }
  }

  def getRoles(entity: Map[String, String]): Option[Seq[String]] = {
    val roles = entity.get("4").map(value => Seq(relator.getOrElse(value, ""))).getOrElse(Seq.empty)

    if (roles.exists(_.nonEmpty)) {
      val item = entity.get("5").map(value => Seq(value)).getOrElse(Seq.empty)
      val allRoles = roles ++ item
      Some(allRoles)
    } else {
      None
    }
  }



  def getSubjectPerson(record: VuFindSolrRecord): Option[Entities] = {
    val persons = record.getFieldsAsMap("600")

    val entities = for {
      entity <- persons
      if !entity.contains("t")
    } yield {
          val name = getPersonName(entity)
          val date = entity.getOrElse("d", None) match {
            case None => None
            case value => Some(value.toString)
          }
          val identifiers = getIdentifiers(entity)
          Entity(
            name=name,
            date=date,
            identifier=identifiers,
            role=None,
            description = None,
            title=None
          )
        }
    Option.when(entities.nonEmpty)(Entities(entities))
  }


  def getAuthorCorporate(record: VuFindSolrRecord): Option[Entities] = {
    val corporates = record.getFieldsAsMap("110") ++ record.getFieldsAsMap("710")
    val otherNormFiles = record.getFieldsAsMap("880")

    val entities = for {
      entity <- corporates
      if !entity.contains("t")
    } yield {
          val name = getCorporateName(entity)
          val description = entity.get("g").map(value => Seq(value))
          val identifiers = getIdentifiers(entity, otherNormFiles)
          val roles = getRoles(entity)
          Entity(
            name=name,
            date=None,
            identifier=identifiers,
            role=roles,
            description = description,
            title=None
          )
        }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getSubjectCorporate(record: VuFindSolrRecord): Option[Entities] = {
    val corporates = record.getFieldsAsMap("610")

    val entities = for {
      entity <- corporates
      if !entity.contains("t")
    } yield {
          val name = getCorporateName(entity)
          val description = entity.get("g").map(value => Seq(value))
          val identifiers = getIdentifiers(entity)
          Entity(
            name=name,
            date=None,
            identifier=identifiers,
            role=None,
            description = description,
            title=None
          )

      }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getAuthorConference(record: VuFindSolrRecord): Option[Entities] = {
    val conferences = record.getFieldsAsMap("111") ++ record.getFieldsAsMap("711")
    val otherNormFiles = record.getFieldsAsMap("880")

    val entities = for {
      entity <- conferences
      if !entity.contains("t")
    } yield {
          val name = getConferenceName(entity)
          val date = entity.getOrElse("d", None) match {
            case None => None
            case value => Some(value.toString)
          }
          val description = entity.get("g").map(value => Seq(value))
          val identifiers = getIdentifiers(entity, otherNormFiles)
          val roles = getRoles(entity)
          Entity(
            name=name,
            date=date,
            identifier=identifiers,
            role=roles,
            description = description,
            title=None
          )

    }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getSubjectConference(record: VuFindSolrRecord): Option[Entities] = {
    val conferences = record.getFieldsAsMap("611")

    val entities = for {
      entity <- conferences
      if !entity.contains("t")
    } yield {
          val name = getConferenceName(entity)
          val date = entity.getOrElse("d", None) match {
            case None => None
            case value => Some(value.toString)
          }
          val description = entity.get("g").map(value => Seq(value))
          val identifiers = getIdentifiers(entity)
          Entity(
            name = name,
            date = date,
            identifier = identifiers,
            role = None,
            description = description,
            title = None
          )


    }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getWork(record: VuFindSolrRecord): Option[Entities] = {
    val works = record.getFieldsAsMap("130") ++ record.getFieldsAsMap("730")
    val personWorks = record.getFieldsAsMap("700")
    val corporateWorks = record.getFieldsAsMap("710")
    val conferenceWorks = record.getFieldsAsMap("711")
    val otherNormFiles = record.getFieldsAsMap("880")

    val workEntities = {
      for (entity <- works) yield {
        val title = getWorkTitle(entity)
        val identifiers = getIdentifiers(entity, otherNormFiles)
        Entity(
          name = None,
          date = None,
          identifier = identifiers,
          role = None,
          description = None,
          title = title
        )
      }
    }

    val personWorkEntities = for {
        entity <- personWorks
        if entity.contains("t")
      } yield {
        val name = getPersonWorkName(entity)
        val title = getWorkTitleT(entity)
        val identifiers = getIdentifiers(entity, otherNormFiles)
        Entity(
          name = name,
          date = None,
          identifier = identifiers,
          role = None,
          description = None,
          title = title
        )
      }

    val corporateWorkEntities = for {
      entity <- corporateWorks
      if entity.contains("t")
    } yield {
      val name = getCorporateName(entity)
      val title = getWorkTitleT(entity)
      val identifiers = getIdentifiers(entity, otherNormFiles)
      Entity(
        name = name,
        date = None,
        identifier = identifiers,
        role = None,
        description = None,
        title = title
      )
    }

    val conferenceWorkEntities = for {
      entity <- conferenceWorks
      if entity.contains("t")
    } yield {
      val name = getConferenceName(entity)
      val title = getWorkTitleT(entity)
      val identifiers = getIdentifiers(entity, otherNormFiles)
      Entity(
        name = name,
        date = None,
        identifier = identifiers,
        role = None,
        description = None,
        title = title
      )
    }


    val result = workEntities ++ personWorkEntities ++ corporateWorkEntities ++ conferenceWorkEntities
    Option.when(result.nonEmpty)(Entities(result))
  }

  def getSubjectWork(record: VuFindSolrRecord): Option[Entities] = {
    val works = record.getFieldsAsMap("630")
    val personWorks = record.getFieldsAsMap("600")
    val corporateWorks = record.getFieldsAsMap("610")
    val conferenceWorks = record.getFieldsAsMap("611")

    val workEntities = {
      for (entity <- works) yield {
        val title = getWorkTitle(entity)
        val identifiers = getIdentifiers(entity)
        Entity(
          name=None,
          date=None,
          identifier=identifiers,
          role=None,
          description = None,
          title=title
        )
      }
    }

    val personWorkEntities = for {
        entity <- personWorks
        if entity.contains("t")
      } yield {
          val name = getPersonWorkName(entity)
          val title = getWorkTitleT(entity)
          val identifiers = getIdentifiers(entity)
          Entity(
            name=name,
            date=None,
            identifier=identifiers,
            role=None,
            description = None,
            title=title
          )
      }


    val corporateWorkEntities = for {
        entity <- corporateWorks
        if entity.contains("t")
      } yield {
          val name = getCorporateName(entity)
          val title = getWorkTitleT(entity)
          val identifiers = getIdentifiers(entity)
          Entity(
            name=name,
            date=None,
            identifier=identifiers,
            role=None,
            description = None,
            title=title
          )
      }

    val conferenceWorkEntities = for {
        entity <- conferenceWorks
        if entity.contains("t")
      } yield {
          val name = getConferenceName(entity)
          val title = getWorkTitleT(entity)
          val identifiers = getIdentifiers(entity)
          Entity(
            name=name,
            date=None,
            identifier=identifiers,
            role=None,
            description = None,
            title=title
          )
      }

    val result = workEntities ++ personWorkEntities ++ corporateWorkEntities ++ conferenceWorkEntities
    Option.when(result.nonEmpty)(Entities(result))
  }


  def getPlace(record: VuFindSolrRecord): Option[Entities] = {
    val places = record.getFieldsAsMap("751")

    val entities = {
      for (entity <- places) yield {
        val name = entity.getOrElse("a", None) match {
          case None => None
          case value => Some(value.toString)
        }
        val description = entity.get("g").map(value => Seq(value))
        val identifiers = getIdentifiers(entity)
        Entity(
          name=name,
          date=None,
          identifier=identifiers,
          role=None,
          description = description,
          title=None
        )
      }
    }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getSubjectPlace(record: VuFindSolrRecord): Option[Entities] = {
    val places = record.getFieldsAsMap("651")

    val entities = {
      for (entity <- places) yield {
        val name = entity.getOrElse("a", None) match {
          case None => None
          case value => Some(value.toString)
        }
        val description = entity.get("g").map(value => Seq(value))
        val identifiers = getIdentifiers(entity)
        Entity(
          name=name,
          date=None,
          identifier=identifiers,
          role=None,
          description = description,
          title=None
        )
      }
    }
    Option.when(entities.nonEmpty)(Entities(entities))
  }


  def getTopic(record: VuFindSolrRecord): Option[Entities] = {
    val topics = record.getFieldsAsMap("650")

    val entities = {
      for (entity <- topics) yield {
        val name = Some(getFormattedField(entity, ListMap(
          "a" -> "",
          "g" -> ", ",
          "x" -> ", ",
        )))
        val identifiers = getIdentifiers(entity)
        Entity(
          name=name,
          date=None,
          identifier=identifiers,
          role=None,
          description = None,
          title=None
        )
      }
    }
    Option.when(entities.nonEmpty)(Entities(entities))
  }

  def getTime(record: VuFindSolrRecord): Option[MultivaluedField] = {
    record.getSubfields("648", "a")  match {
      case Some(values) => Some(MultivaluedField(values))
      case None => None
    }
  }


  def getSubjectMusic(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("655")

    val values = {
      for (field <- fields) yield {
        if (field.contains("2") && field.getOrElse("2", "") == "idsmusg") {
          getFormattedField(field, ListMap("a" -> "", "y" -> ", ", "z" -> ", ", "v" -> ", "))
        }
        else ""
      }
    }

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getForm(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val gndContent = getSingleSubfieldSource(record, "655", "a", "gnd-content")
    val gndCarrier = getSingleSubfieldSource(record, "655", "a", "gnd-carrier")
    val uzbZK = getSingleSubfieldSource(record, "965", "a", "uzb-ZK")
    val uzbZG = getSingleSubfieldSource(record, "965", "a", "uzb-ZG")
    val uzbZH = getSingleSubfieldSource(record, "965", "a", "uzb-ZH")
    val gndMusic = getSingleSubfieldSource(record, "348", "a", "gnd-music")

    val values = gndContent ++ gndCarrier ++ uzbZK ++ uzbZG ++ uzbZH ++ gndMusic

    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getDateNormalized(record: VuFindSolrRecord): Option[SingleField] = {
    val dateExact = record.getFieldsAsMap("046")

    if (dateExact.length == 1) {
      Some(SingleField(exactUnitDate(dateExact.head)))
    }
    else {
      Some(SingleField(yearUnitDate(record)))
    }
  }


  def getContents(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val listTableOfContents = record.getFieldsAsList("505")

    val tableOfContents = {
      for (listTableOfContent <- listTableOfContents) yield {
        listTableOfContent.filterNot(_._1 == "6").map(_._2.replace("<<", "").replace(">>", "")).mkString(", ")
      }
    }

    val hanContents = getAllSubfields(record, "596", "3")
    val values = tableOfContents ++ hanContents
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getContentsScript(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val fields = record.getFieldsAsMap("880")
    val fieldsList = record.getFieldsAsList("880")

    val listTableOfContents = {
      for {
        field <- fieldsList
        if field.exists{ case (key, value) => key == "6" && value.startsWith("505-") }
      } yield field.filterNot(_._1 == "6")
    }

    val tableOfContents = {
      for (listTableOfContent <- listTableOfContents) yield {
        listTableOfContent.map(_._2.replace("<<", "").replace(">>", "")).mkString(", ")
      }
    }



    val hanNotes = fields.filter(f => f.getOrElse("6", "").startsWith("596-"))
    val hanContents = {
      for {
        field <- hanNotes
        sfValues = field.view.filterKeys(key => !Set("x", "9", "6", "2", "0").contains(key)).values.toSeq
      } yield sfValues.mkString(", ")
    }

    val values = tableOfContents ++ hanContents
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def getThumbnail(record: VuFindSolrRecord): Option[MultivaluedField] = {
    val dois = getSingleSubfieldSource(record, "024", "a", "doi")

    val values = {
      for (doi <- dois) yield {
        if (doi.startsWith("10.3931/e-rara")) {
          s"https://www.e-rara.ch/titlepage/doi/$doi/128"
        }
        else if (doi.startsWith("10.7891/e-manuscripta")) {
          s"https://www.e-manuscripta.ch/titlepage/doi/$doi/128"
        }
        else ""
      }
    }
    Option.when(values.nonEmpty)(MultivaluedField(values.filter(_.nonEmpty)))
  }

  def exactUnitDate(date: Map[String, String]): String = {
    val startDateBC = cleanDate(date.getOrElse("b", ""))
    val startDate = cleanDate(date.getOrElse("c", ""))
    val endDateBC = cleanDate(date.getOrElse("d", ""))
    val endDate = cleanDate(date.getOrElse("e", ""))

    val dateNorm = {
      if (startDateBC.nonEmpty) {
        "-" + startDateBC +
          (if (endDateBC.nonEmpty) "/" + "-" + endDateBC
          else if (endDate.nonEmpty) "/" + endDate
          else "")
      }
      else if (startDate.nonEmpty) {
        startDate +
          (if (endDateBC.nonEmpty) "/" + "-" + endDateBC
          else if (endDate.nonEmpty) "/" + endDate
          else "")
      }
      else if (endDateBC.nonEmpty) {
        "-" + endDateBC
      }
      else if (endDate.nonEmpty) {
        endDate
      }
      else ""
    }

    dateNorm

  }

  def yearUnitDate(record: VuFindSolrRecord): String = {
    val startYear = {
      record.getControlfield("008").substring(7, 11)
    }
    val endYear = {
      record.getControlfield("008").substring(11, 15)
    }

    val dateNorm = {
      if (startYear != "----" && startYear != "uuuu" && startYear != "    ") {
        startYear +
          (if (endYear != "----" && endYear != "uuuu" && endYear != "    ") "/" + endYear else "")
      }
      else if (endYear != "----" && endYear != "uuuu" && endYear != "    ") {
        endYear
      }
      else ""
    }

    dateNorm
  }

  def cleanDate(rawDate: String): String = {
    val date = rawDate.replace(".", "").replace("[", "").replace("]", "")
    date match {
      case date if date.length == 6 && date.substring(4, 6).toInt > 12 => date.substring(0, 4)
      case date if date.length == 6 => date.substring(0, 4) + "-" + date.substring(4, 6)
      case date if date.length == 8 && date.endsWith("0000") => date.substring(0, 4)
      case date if date.length == 8 && date.endsWith("00") => date.substring(0, 4) + "-" + date.substring(4, 6)
      case _ => date
    }
  }

  /**
   * @param record a VuFindSolrRecord
   * @param field a MARC field
   * @param subfield a MARC subfield
   * @return a string concatenated with | of all the content in the given subfield occurring in the same or in multiple fields
   */
  def getSingleSubfield(record: VuFindSolrRecord, field: String, subfield: String): String = {
    val content = record.getSubfields(field, subfield)
    content match {
      case Some(value) => value.mkString("|")
      case None => ""
    }
  }

  def getSingleSubfieldSource(record: VuFindSolrRecord, field: String, subfield: String, source: String): Seq[String] = {
    val fields = record.getFieldsAsMap(field)

    val value = {
      for (field <- fields) yield {
        if (field.contains("2") && field.getOrElse("2", "") == source) {
          field.getOrElse(subfield, "")
        }
        else ""
      }
    }

    value.filter(_.nonEmpty)

  }

  def getAuthorsString(record: VuFindSolrRecord, field: String, subfield: String, relator: String): String = {
    val fields = record.getFieldsAsMap(field)

    val value = {
      for (field <- fields) yield {
        if (field.contains("4") && field.getOrElse("4", "").contains(relator)) {
          field.getOrElse(subfield, "").replace("<<", "").replace(">>", "")
        }
        else ""
      }
    }

    if (value.nonEmpty) value.filter(_.nonEmpty).mkString(" ; ")
    else ""
  }

  def getAllSubfields(record: VuFindSolrRecord, field: String): Seq[String] = {
    val fields: Seq[Map[String, String]] = record.getFieldsAsMap(field)

    val value = {
      for {
        field <- fields
        sfValues = field.view.filterKeys(key => !Set("x", "9", "6", "2", "0").contains(key)).values.toSeq
      } yield sfValues.mkString(", ")
    }

    value.filter(_.nonEmpty)
  }

  def getAllSubfields(record: VuFindSolrRecord, field: String, ind1: String): Seq[String] = {
    val fields: Seq[Map[String, String]] = record.getFieldsAsMap(field, ind1)

    val value = {
      for {
        field <- fields
        sfValues = field.view.filterKeys(key => !Set("x", "9", "6", "2", "0").contains(key)).values.toSeq
      } yield sfValues.mkString(", ")
    }

    value.filter(_.nonEmpty)
  }

  def getHanNote(record: VuFindSolrRecord, ind1: String, subfield: String): Seq[String] = {
    val fields = record.getFieldsAsMap("596", ind1)

    val value = {
      for (field <- fields) yield {
        if (field.contains(subfield)) {
          getFormattedField(field, ListMap("3" -> "", subfield -> " : "))
        }
        else ""
      }
    }

    value.filter(_.nonEmpty)
  }

  /**
   * @param subfields a map of of subfields with their values
   * @param specs a map of subfields with the delimiter preceding the subfield
   * @return a concatenated string with the values and delimiters according to subfield
   */
  def getFormattedField(subfields: Map[String, String], specs: ListMap[String, String]): String = {
    var previousValueWasEmpty = true
    var formattedValues: List[String] = List()
    for ((key, sep) <- specs) {
      subfields.get(key).foreach { value =>
        val formattedValue = if (previousValueWasEmpty) value else s"$sep$value" + (if (sep == " (") ")" else "")
        previousValueWasEmpty = formattedValue.isEmpty
        formattedValues = formattedValue :: formattedValues
      }
    }
    formattedValues.reverse.mkString("")
  }


}


