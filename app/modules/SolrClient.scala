package modules


import ch.swisscollections.AppSettings
import com.google.inject.{Inject, Provider, Singleton}
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.SolrClient
import play.api.Configuration

@Singleton
class SolrClientProvider @Inject()(config: Configuration) extends Provider[SolrClient] with AppSettings {
  override def get(): SolrClient = {
    new HttpSolrClient.Builder(nodeURL)
      .withConnectionTimeout(connectionTimeout.toInt)
      .withSocketTimeout(socketTimeout.toInt)
      .build()
  }
}
