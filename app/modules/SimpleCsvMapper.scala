/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package modules

import scala.collection.immutable.ListMap
import scala.collection.mutable
import scala.language.postfixOps

class SimpleCsvMapper {


  val libraryName: Map[String, String] = Map(
    "A100" -> "Basel, Universitätsbibliothek",
    "A115" -> "Basler Bibliographie",
    "A116" -> "Basel, UB, Verzeichnis der Basler Drucke",
    "A117" -> "Bernoulli-Briefinventar",
    "A118" -> "Autographensammlung Geigy-Hagenbach",
    "A125" -> "Basel, Schweiz. Wirtschaftsarchiv",
    "A150" -> "Solothurn, Zentralbibliothek",
    "A380" -> "Beromünster, Stiftsbibliothek",
    "A381" -> "Frauenfeld, Kantonsbibliothek Thurgau",
    "A382" -> "Zofingen, Stadtbibliothek",
    "AKB" -> "Aarau, Aargauer Kantonsbibliothek",
    "B400" -> "Bern, UB Speichermagazin",
    "B404" -> "Bern, UB Münstergasse",
    "B410" -> "Bern, UB Unitobler",
    "B412" -> "Bibliographie der Berner Geschichte",
    "B415" -> "Bern, UB SOB",
    "B452" -> "Bern, UB Medizin",
    "B464" -> "Bern, UB JFB",
    "B465" -> "Bern, UB JBB",
    "B466" -> "Bern, UB Eugen Huber Bibliothek",
    "B467" -> "Bern, UB Geografie",
    "B500" -> "Bern, UB vonRoll",
    "B521" -> "Bern, UB Zahnmedizin",
    "B542" -> "Bern, Uni UPD WA",
    "B552" -> "Bern, UB Pflanzenwissenschaften",
    "B554" -> "Bern, UB Muesmatt",
    "B555" -> "Bern, UB Mittelstrasse",
    "B583" -> "Bern, UB Medizingeschichte",
    "B583RO" -> "Bern, IMG, Archiv Hermann Rorschach",
    "B589" -> "Bern, UB Wirtschaft",
    "E30" -> "Bern, UB Exakte Wissenschaften",
    "HAN001" -> "Uni Bern - Archiv und Sammlung Rorschach",
    "LUDOL" -> "Haus zum Dolder, Beromünster",
    "LUKIL" -> "ZHB Luzern - UNI/PH-Gebäude - RPI",
    "LULES" -> "ZHB Luzern - Sempacherstrasse",
    "LUNI3" -> "ZHB Luzern - UNI/PH-Gebäude - Fak. III",
    "LUPHL" -> "ZHB Luzern - UNI/PH-Gebäude - PHZ",
    "LUSBI" -> "Luzern, ZHB",
    "LUUHL" -> "ZHB Luzern - Uni/PH-Gebäude",
    "LUZHB" -> "Luzern, ZHB",
    "SGARK" -> "Trogen, Kantonsbibliothek Ausserrhoden",
    "SGKBN" -> "KB Vadiana St. Gallen - Naturhistorisches Museum",
    "SGKBV" -> "St. Gallen, Kantonsbibliothek Vadiana St. Gallen",
    "SGKHI" -> "KB Vadiana St. Gallen - Historisches Museum",
    "SGSTI" -> "St. Gallen, Stiftsbibliothek",
    "SGZFB" -> "KB Vadiana - St. Galler Zentrum für das Buch",
    "UMWI" -> "Zürich, ZB, Musikwissenschaftliches Institut der UZH",
    "Z01" -> "Zürich, ZB",
    "Z02" -> "Zürich, ZB, Graphische Sammlung",
    "Z03" -> "Zürich, ZB, Handschriftenabteilung",
    "Z04" -> "Zürich, ZB, Kartensammlung",
    "Z05" -> "Zürich, ZB, Musikabteilung",
    "Z06" -> "Zürich, ZB, Alte Drucke und Rara",
    "Z07" -> "Zürich, ZB, Bibliothek Schlag",
    "Z08" -> "Zürich, ZB, Verlagsbucharchiv",
    "UIPZ" -> "Zürich, ZB",
    "UDS" -> "Zürich, ZB",
    "UFBI" -> "Zürich, ZB",
    "UKHIS" -> "Zürich, ZB",
    "UHS" -> "Zürich, ZB",
  )

  // Hash with concordance MARC21 language codes and written language name
  val language: Map[String, String] = Map(
    "aar" -> "Afar",
    "afr" -> "Afrikaans",
    "alb" -> "Albanisch",
    "ang" -> "Altenglisch",
    "ara" -> "Arabisch",
    "arc" -> "Aramäisch",
    "arm" -> "Armenisch",
    "aze" -> "Azeri",
    "baq" -> "Baskisch",
    "bel" -> "Weissrussisch",
    "ben" -> "Bengali",
    "bos" -> "Bosnisch",
    "bul" -> "Bulgarisch",
    "bur" -> "Burmesisch",
    "chi" -> "Chinesisch",
    "chu" -> "Altbulgarisch, Kirchenslawisch",
    "cop" -> "Koptisch",
    "cze" -> "Tschechisch",
    "dan" -> "Dänisch",
    "dut" -> "Niederländisch",
    "egy" -> "Ägyptisch",
    "eng" -> "Englisch",
    "est" -> "Estnisch",
    "fin" -> "Finnisch",
    "fre" -> "Französisch",
    "geo" -> "Georgisch",
    "ger" -> "Deutsch",
    "gez" -> "Äthiopisch",
    "gla" -> "Gälisch",
    "gle" -> "Gälisch",
    "grc" -> "Altgriechisch",
    "gre" -> "Neugriechisch",
    "gsw" -> "Schweizerdeutsch",
    "heb" -> "Hebräisch",
    "hin" -> "Hindi",
    "hrv" -> "Kroatisch",
    "hun" -> "Ungarisch",
    "ice" -> "Isländisch",
    "ind" -> "Indonesisch",
    "ita" -> "Italienisch",
    "jav" -> "Javanisch",
    "jpn" -> "Japanisch",
    "kas" -> "Kashmiri",
    "kaz" -> "Kasachisch",
    "khm" -> "Khmer",
    "kir" -> "Kirisisch",
    "kor" -> "Koreanisch",
    "kur" -> "Kurdisch",
    "lat" -> "Lateinisch",
    "lav" -> "Lettisch",
    "lit" -> "Litauisch",
    "mac" -> "Mazedonisch",
    "may" -> "Malaiisch",
    "mon" -> "Mongolisch",
    "mul" -> "Mehrere Sprachen",
    "nor" -> "Norwegisch",
    "ota" -> "Osmanisch",
    "per" -> "Persisch",
    "pol" -> "Polnisch",
    "por" -> "Portugiesisch",
    "roh" -> "Rätoromanisch",
    "rom" -> "Romani",
    "rum" -> "Rumänisch",
    "rus" -> "Russisch",
    "san" -> "Sanskrit",
    "slo" -> "Slowakisch",
    "slv" -> "Slowenisch",
    "spa" -> "Spanisch",
    "srp" -> "Serbisch",
    "swa" -> "Swahili",
    "swe" -> "Schwedisch",
    "syr" -> "Syrisch",
    "tam" -> "Tamil",
    "tgk" -> "Tadschikisch",
    "tgl" -> "Philippinisch",
    "tha" -> "Siamesisch",
    "tuk" -> "Turkmenisch",
    "tur" -> "Türkisch",
    "ukr" -> "Ukrainisch",
    "urd" -> "Urdu",
    "uzb" -> "Usbekisch",
    "vie" -> "Vietnamisch",
    "wen" -> "Sorbisch",
    "yid" -> "Jiddisch"
  )

  //these are the column titles of the csv and the corresponding functions
  private val columns: ListMap[String, (VuFindSolrRecord) => String] = ListMap(
    "\"Bibliothek, Signatur\"" -> getLibraryCallNo, // 852 $b (translated), $j
    "\"Titel\"" -> getTitle, // 245 $a / $c
    "\"Veröffentlichungsangabe\"" -> getPublicationStatement, // 264 $a, $c
    "\"Physische Beschreibung\"" -> getPhysicalDescription, // 300 $a : $b ; $c + $e ($3)
    "\"Physische Beschaffenheit\"" -> getMedium, // 340 $a : $b ; $c + $e ($i)
    "\"Verzeichnungsstufe\"" -> getDescriptionLevel, // 351 $c
    "\"Sprache, Schrift\"" -> getLanguage, // 546, 008
    "\"Digitalisat\"" -> getLinks, // 856 41 $u, 8564# $u, AVD $u
    "\"Identifier swisscollections\"" -> getId,
    "\"Identifier swisscollections for Excel\"" -> getIdExcel,
  )

  def mapToCsv(record: VuFindSolrRecord): String = {
    val resultList: List[String] = {
      for ((_, columnFunction) <- columns) yield {
        val res = columnFunction(record)
        res
      }
    }.toList

    resultList.map(
      field => field.replaceAll("\"", "\"\"") //escape " in csv with ""
    ).mkString("\"","\";\"", "\"") //create csv line
  }

  def getCsvHeader: String = {
    columns.keys.mkString(";") + "\n"
  }

  def getId(record: VuFindSolrRecord): String = {
    record.getControlfield("001")
  }

  def getIdExcel(record: VuFindSolrRecord): String = {
    val id = record.getControlfield("001")
    s"=\"$id\""
  }

  def getTitle(record: VuFindSolrRecord): String = {
    val title = record.getFirstSubfield("245", "a").getOrElse("").replace("<", "").replace(">","")
    val statementOfResponsibility = record.getFirstSubfield("245", "c").getOrElse("")

    if (statementOfResponsibility.nonEmpty) {
      s"$title / $statementOfResponsibility"
    } else {
      title
    }
  }

  def getPublicationStatement(record: VuFindSolrRecord): String = {
    val publicationStatement = record.getFieldsAsMapInd("264", "0") ++
      record.getFieldsAsMapInd("264", "1") ++
      record.getFieldsAsMapInd("264", "2") ++
      record.getFieldsAsMapInd("264", "3")

    val value = {
      for (statement <- publicationStatement) yield {
        getFormattedField(statement, ListMap("a" -> "", "c" -> ", "))
      }
    }

    value.mkString(" ; ")
  }

 def getPhysicalDescription(record: VuFindSolrRecord): String = {
    val physicalDescription = record.getFieldsAsMap("300")

  val value = {
    for (description <- physicalDescription) yield {
      val string = getFormattedField(description, ListMap("a" -> "", "b" -> " : ", "c" -> " ; ", "e" -> " + "))

      if (description.contains("3") && string.nonEmpty) string + " (" + description.getOrElse("3", "") + ")"
      else if (description.contains("3")) "(" + description.getOrElse("3", "") + ")"
      else string
    }
  }

  value.mkString(" ; ")
}

  def getMedium(record: VuFindSolrRecord): String = {
    val physicalDescription = record.getFieldsAsMap("340")

    val value = {
      for (description <- physicalDescription) yield {

        val string = getFormattedField(description, ListMap("a" -> "", "b" -> " : ", "c" -> " ; ", "e" -> " + "))

        if (description.contains("i") && string.nonEmpty) string + " (" + description.getOrElse("i", "") + ")"
        else if (description.contains("i")) "(" + description.getOrElse("i", "") + ")"
        else string
      }
    }

    value.mkString(" ; ")
  }

  def getLanguage(record: VuFindSolrRecord): String = {
    val languageNote = record.getFieldsAsMap("546")
    val languageCodes = (Set(record.getControlfield("008").substring(35, 38).replace("   ", "und").replace("zxx", "und")) ++
      record.getSubfields("041", "a").getOrElse(Set()))
      .map(code => language.getOrElse(code, ""))

    val value = {
      for (note <- languageNote) yield {
        getFormattedField(note, ListMap("a" -> "", "b" -> ", "))
      }
    }

    if (value.nonEmpty) value.mkString(" ; ") else languageCodes.filter(_.nonEmpty).mkString(", ")
  }

  def getInstitution(record: VuFindSolrRecord): String = {
    val institutions = record.getSubfields("852", "b")
    institutions match {
      case Some(value) => value.mkString("###")
      case None => ""
    }
  }

  def getLinks(record: VuFindSolrRecord): String = {
    val links = record.getFieldsAsMap("856", "4", " ") ++
      record.getFieldsAsMap("856", "4", "1") ++
      record.getFieldsAsMap("AVD")

    val value = {
      for (link <- links) yield {
        link.getOrElse("u", "")
      }
    }

    value.mkString(" ; ")

  }

  def getLibraryCallNo(record: VuFindSolrRecord): String = {
    val holdings = record.getFieldsAsMap("852")

    val value = {
      for (holding <- holdings) yield {
        libraryName(holding.getOrElse("b", "")) +
          (if (holding.contains("j")) ", " + holding.getOrElse("j", "") else "")
      }
    }

    value.mkString(" ; ")

  }

  def getDescriptionLevel(record: VuFindSolrRecord): String = {
    val descriptionLevel = record.getFirstSubfield("351", "c")
    descriptionLevel match {
      case Some(value) => value
      case None => ""
    }
  }

  /**
   * @param subfields a map of of subfields with their values
   * @param specs a map of subfields with the delimiter preceding the subfield
   * @return a concatenated string with the values and delimiters according to subfield
   */
  def getFormattedField(subfields: Map[String, String], specs: ListMap[String, String]): String = {
    var previousValueWasEmpty = true
    var formattedValues: List[String] = List()
    for ((key, sep) <- specs) {
      subfields.get(key).foreach { value =>
        val formattedValue = if (previousValueWasEmpty) value else s"$sep$value" + (if (sep == " (") ")" else "")
        previousValueWasEmpty = formattedValue.isEmpty
        formattedValues = formattedValue :: formattedValues
      }
    }
    formattedValues.reverse.mkString("")
  }

}
