/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package modules

import org.apache.solr.common.SolrDocument

import scala.xml.{Elem, NodeSeq, XML}

class VuFindSolrRecord(solrDocument: SolrDocument) extends SolrDocument(solrDocument: SolrDocument){

  def xmlFullRecord: Elem = XML.loadString(solrDocument.getFieldValue("fullrecord").toString)

  //returns a sequence of the corresponding subfields
  def getSubfields(field: String, subfield: String): Option[Seq[String]] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res)
    } else {
      None
    }
  }

  //returns the first subfield value
  //otherwise return emtpy string
  def getFirstSubfield(field: String, subfield: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if df \@ "tag" == field
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res.head)
    } else {
      None
    }
  }

  //returns a sequence of the corresponding subfields
  //returns an empty list if there are no matching subfields
  def getSubfields(field: String, subfield: String, ind2: String): Option[Seq[String]] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind2" == ind2)
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res)
    } else {
      None
    }
  }

  //returns a sequence of the corresponding subfields
  //returns an empty list if there are no matching subfields
  def getSubfields(field: String, subfield: String, ind1: String, ind2: String): Option[Seq[String]] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1 && df \@ "ind2" == ind2)
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res)
    } else {
      None
    }
  }

  //returns the first subfield value
  //otherwise return emtpy string
  def getFirstSubfield(field: String, subfield: String, ind1: String, ind2: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1 && df \@ "ind2" == ind2)
      sf <- df \\ "subfield"
      if sf \@ "code" == subfield
    } yield sf.text
    if (res.nonEmpty) {
      Some(res.head)
    } else {
      None
    }
  }

  //returns the value of the first matching controlfield
  def getControlfield(controlfield: String): String = {
    val res = for {
      df <- xmlFullRecord \\ "controlfield"
      if df \@ "tag" == controlfield
    } yield df
    res.head.text
  }

  def getControlfieldOption(controlfield: String): Option[String] = {
    val res = for {
      df <- xmlFullRecord \\ "controlfield"
      if df \@ "tag" == controlfield
    } yield df.text
    res.headOption
  }

  /* all subfields values */
  def getFieldsAsList(field: String): Seq[List[(String, String)]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field)
    } yield (convertFieldToList(df))
    fieldNodes
  }

  /* all values per subfield code concatenated */
  def getFieldsAsMap(field: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  /* all values per subfield code concatenated */
  def getFieldsAsMap(field: String, ind1: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  /* only one value (the last one) per subfield code */
  def getFieldsAsMapInd(field: String, ind2: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind2" == ind2)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  /* only one value (the last one) per subfield code */
  def getFieldsAsMap(field: String, ind1: String, ind2: String): Seq[Map[String, String]] = {
    val fieldNodes = for {
      df <- xmlFullRecord \\ "datafield"
      if (df \@ "tag" == field && df \@ "ind1" == ind1 && df \@ "ind2" == ind2)
    } yield convertFieldToMap(df)
    fieldNodes
  }

  //todo : currently if a field has twice the same subfield, only one value is retained !!!
  //def convertFieldToMap(field: NodeSeq): Map[String, String] ={
  //  var res = Map[String,String]()
  //  for (sf <- field \\ "subfield") {
  //    res += (sf \@ "code" -> sf.text)
  //  }
  //  res
  //}

  // should work for repeated subfields ; repeated subfields are concatenated with comma
  def convertFieldToMap(field: NodeSeq): Map[String, String] = {
    (field \\ "subfield").collect {
      case sf: Elem => (sf \@ "code", sf.text)
    }.groupBy(_._1).map {
      case (code, values) => (code, values.map(_._2).mkString(", "))
    }
  }

  def getHostItemAsMap(field: String): Seq[Map[String, String]] = {

      val fieldNodes = for {
        df <- xmlFullRecord \\ "datafield"
        if (df \@ "tag" == field)
      } yield {
        (df \\ "subfield").collect {
          case sf: Elem if sf \@ "code" == "g" =>
            val text = sf.text
            if (text.startsWith("yr:")) Some("yr" -> text.stripPrefix("yr:"))
            else if (text.startsWith("no:")) Some("no" -> text.stripPrefix("no:"))
            else Some((sf \@ "code", sf.text))
          case sf: Elem => Some((sf \@ "code", sf.text))
        }.flatten.groupBy(_._1).map {
          case (code, values) => (code, values.map(_._2).mkString(", "))
        }
      }

      fieldNodes

  }

  def convertFieldToList(field: NodeSeq): List[(String, String)] ={
    var res = List[(String,String)]()
    for (sf <- field \\ "subfield") {
      res = res :+ (sf \@ "code", sf.text)
    }
    res
  }
}
