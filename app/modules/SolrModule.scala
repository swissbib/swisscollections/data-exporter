package modules


import com.google.inject.AbstractModule
import org.apache.solr.client.solrj.SolrClient

class SolrModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[SolrClient]).toProvider(classOf[SolrClientProvider])
  }
}
