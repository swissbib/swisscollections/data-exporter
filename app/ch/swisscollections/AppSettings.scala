package ch.swisscollections

trait AppSettings {

  val nodeURL: String = sys.env.getOrElse("NODE_URL", "https://solrtest.swisscollections.unibas.ch/solr/")
  val solrCollection: String = sys.env.getOrElse("SOLR_COLLECTION", "swisscollectionstest")

  val connectionTimeout: String = sys.env.getOrElse("CONNECTION_TIMEOUT", "20000000")
  val socketTimeout: String = sys.env.getOrElse("SOCKET_TIMEOUT", "60000000")

  val staticFilesFolder: String = sys.env.getOrElse("STATIC_FILES_FOLDER", "/home/lionel/Documents/mycloud/arbim/git_repo/swisscollections/data-exporter/conf/static-files")
}
