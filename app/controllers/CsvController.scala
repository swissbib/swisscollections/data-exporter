/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */package controllers

import akka.stream.scaladsl.{Concat, Source}

import org.apache.solr.client.solrj.SolrClient
import org.apache.solr.client.solrj.SolrQuery
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.response.QueryResponse
import org.apache.solr.common.SolrDocument
import play.api.mvc._
import akka.util.ByteString
import ch.swisscollections.AppSettings
import modules.{SimpleCsvMapper, VuFindSolrRecord}
import org.apache.solr.common.params.MultiMapSolrParams
import org.apache.solr.servlet.SolrRequestParsers.parseQueryString
import play.api.http.{HttpChunk, HttpEntity}

import javax.inject.Inject
import scala.concurrent.Future
import scala.jdk.CollectionConverters._

class CsvController @Inject()(solrClient: SolrClient, cc: ControllerComponents) extends AbstractController(cc) with AppSettings {

  private val batchSize = 200
  private lazy val simpleCsvMapper = new SimpleCsvMapper
  private lazy val solrParametersToKeep = List(
    "qt",
    "q",
    "fq",
    "qf",
    "mm",
    "q.op",
    "sow",
    "df"
  )


  def fetchSolrRecords(solrQueryString: String, cursorMark: String): QueryResponse = {
    //Todo use a common function for both controllers
    val solrQuery = new SolrQuery()

    val paramsList: MultiMapSolrParams = parseQueryString(solrQueryString)

    val paramMap: Map[String, Array[String]] = paramsList.getMap.asScala.toMap

    //only keep relevant parameters for solr
    val filteredParams: Map[String, Array[String]] = paramMap.filter { case (k, _) => solrParametersToKeep.contains(k) }

    val solrParams = new MultiMapSolrParams(filteredParams.asJava)

    solrQuery.add(solrParams)


    solrQuery.setRows(batchSize)
    solrQuery.setSort("id", SolrQuery.ORDER.asc)
    solrQuery.setFields("id, title, institution, fullrecord")
    solrQuery.set("cursorMark", cursorMark)
    solrClient.query(solrCollection, solrQuery)
  }

  /**
   *
   * @param solr_query_string Solr Query String, for example institution%3A"A115" or id%3A991092189179705501
   * @return
   */
  def generateCsvFromSolr(solr_query_string: String): Action[AnyContent] = Action.async { implicit request =>
    val cursorMark = "*"

    // Define the headers for the CSV file
    val headers = Map(
      "Content-Disposition" -> "attachment; filename=swisscollections_export.csv"
    )

    val csvHeader = Source.single(ByteString(simpleCsvMapper.getCsvHeader))
    // Stream CSV data in chunks
    val csvSource: Source[ByteString, _] = Source.unfold(cursorMark) { cursorMark =>
      val response = fetchSolrRecords(solr_query_string, cursorMark)

      val solrRecords: List[SolrDocument] = response.getResults.asScala.toList

      val nextCursorMark = response.getNextCursorMark

      if (solrRecords.isEmpty) {
        None
      } else {
        //we add the header
        val solrRecordsAsCsvLines = solrRecords.map { doc =>
          val vfDoc = new VuFindSolrRecord(doc)
          simpleCsvMapper.mapToCsv(vfDoc)
        }.mkString("","\n","\n")

        val bytes = ByteString.fromString(solrRecordsAsCsvLines)
        Some((nextCursorMark, bytes))
      }
    }

    val bomSource = Source.single(ByteString.apply(0xEF.toByte, 0xBB.toByte, 0xBF.toByte))
    val csvSourceWithHeader = Source.combine(csvHeader, csvSource)(Concat(_))

    // Create a chunked response with CSV chunks
    val result = Result(
      header = ResponseHeader(200, headers),
      body = HttpEntity.Chunked(csvSourceWithHeader.prepend(bomSource).map(bytesSeq => HttpChunk.Chunk(bytesSeq)), Some("text/csv"))
    )
    Future.successful(result)
  }


  def fallback(nonsense: Option[String]): Action[AnyContent] =
    Action { implicit request =>
      Ok(s"swisscollections data/csv export tool, cf. https://ub-basel.atlassian.net/wiki/spaces/swisscollections/pages/2271019009/Data+Export+Best+Practice+Documentation")
    }

}
