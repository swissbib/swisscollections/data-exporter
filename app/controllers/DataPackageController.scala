/*
 * termsquery-analysis-api
 * Copyright (C) 2021  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package controllers

import akka.stream.scaladsl.StreamConverters
import akka.util.ByteString
import ch.swisscollections.AppSettings
import modules.{ExportRecord, FullExportMapper, QueryData, VuFindSolrRecord}
import org.apache.solr.client.solrj.impl.HttpSolrClient
import org.apache.solr.client.solrj.response.QueryResponse
import org.apache.solr.client.solrj.{SolrClient, SolrQuery, SolrServerException}
import org.apache.solr.common.params.{MapSolrParams, ModifiableSolrParams, MultiMapSolrParams}
import org.apache.solr.common.{SolrDocument, SolrException}
import org.apache.solr.servlet.SolrRequestParsers.parseQueryString
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json.{Json, Writes}
import play.api.mvc._
import play.shaded.ahc.io.netty.handler.codec.http.QueryStringDecoder

import java.io.{BufferedInputStream, FileInputStream, IOException, PipedInputStream, PipedOutputStream, UnsupportedEncodingException}
import java.util.zip.{ZipEntry, ZipOutputStream}
import javax.inject.Inject
import scala.io.Source
import scala.jdk.CollectionConverters._
import java.net.{URLDecoder, URLEncoder}

class DataPackageController @Inject()(solrClient: SolrClient, cc: ControllerComponents) extends AbstractController(cc) with AppSettings {

  private val batchSize = 200
  private lazy val fullExportMapper = new FullExportMapper
  private lazy val solrParametersToKeep = List(
    "qt",
    "q",
    "fq",
    "qf",
    "mm",
    "q.op",
    "sow",
    "df"
  )

  /**
   *
   * @param solrQueryString Solr Query String, for example institution%3A"A115" or id%3A991092189179705501
   * @return
   */
  def generateDataPackageFromSolr(solrQueryString: String, vufindUrl: String): Action[AnyContent] = Action { implicit request =>
    val initialCursorMark = "*"


    try {
      val currentTimeStamp = getCurrentTimestamp()

      val rootFolderBaseName = getRootFolderBaseName(solrQueryString).take(30)

      val rootFolderName = (rootFolderBaseName, currentTimeStamp) match {
        case ("", _) => currentTimeStamp
        case _        => s"${rootFolderBaseName}_$currentTimeStamp"
      }

      println("Started Data Package Export from " + vufindUrl)


      val pipedOut = new PipedOutputStream()
      val pipedIn = new PipedInputStream(pipedOut)

      val zipThread = new Thread(() => {
        val zip = new ZipOutputStream(pipedOut)

        var numberOfResults: Long = 0
        var solrRecords: List[SolrDocument] = List()
        var nextCursorMark = initialCursorMark
        var counter = 1 //count iteration on solr requests

        while (counter == 1 || (counter - 1) * batchSize < numberOfResults) {

          //todo : raise exceptions directly in fetchSolrRecords ?
          val response = fetchSolrRecords(solrQueryString, nextCursorMark)

          try {
            solrRecords = response.getResults.asScala.toList
          } catch {
            case e: Exception => {
              println(s"An unexpected error occurred 101: ${e.getMessage}")
              val errorFile = new ZipEntry(s"error.txt")
              zip.putNextEntry(errorFile)
              zip.write(s"Due to an error, the export was interrupted. Details : ${e.getMessage}".getBytes("UTF-8"))
              zip.closeEntry()
              solrRecords = List()
            }
          }

          nextCursorMark = response.getNextCursorMark

          if (counter == 1) {
            numberOfResults = response.getResults.getNumFound
            generateStaticFiles(solrQueryString, vufindUrl, rootFolderName, zip, numberOfResults, currentTimeStamp)
          }

          var (csvFileContent: String, jsFileContent: String, marcXmlFileContent: String, urlCsvFileContent: String, jsonLinesContent: String) = getContentHeader(counter)


          //Logging
          val currentRecordStart = (counter - 1) * batchSize + 1
          val currentRecordEnd = Math.min(counter * batchSize, numberOfResults.toInt)
          println(s"Records $currentRecordStart-$currentRecordEnd out of $numberOfResults")


          //iterate over each record
          for (record <- solrRecords) {
            val vfRecord = new VuFindSolrRecord(record)
            val exportRecord = fullExportMapper.convertToExportRecord(vfRecord)
            csvFileContent = csvFileContent + transformToCsvBytes(exportRecord)
            urlCsvFileContent = urlCsvFileContent + fullExportMapper.generateUrlCsv(exportRecord)
            jsFileContent = jsFileContent + transformToJsBytes(exportRecord)
            marcXmlFileContent = marcXmlFileContent + transformToMarcXmlBytes(vfRecord)
            jsonLinesContent = jsonLinesContent + transformToJsonLines(exportRecord)
          }

          jsFileContent = appendJsFooter(jsFileContent, counter)
          marcXmlFileContent = appendMarcXmlFooter(marcXmlFileContent)

          val formattedCounter = f"$counter%06d" //counter on 6 digits

          addCsvFileToZipStream(zip, s"data/csv/metadata/metadata_$formattedCounter.csv", csvFileContent)
          addCsvFileToZipStream(zip, s"data/csv/media-file-links/media_file_links_$formattedCounter.csv", urlCsvFileContent)
          addFileToZipStream(zip, s"ignore/js/list_$formattedCounter.js", jsFileContent)
          addFileToZipStream(zip, s"data/jsonl/metadata_$formattedCounter.jsonl", jsonLinesContent)
          addFileToZipStream(zip, s"data/marcxml/metadata_$formattedCounter.xml", marcXmlFileContent)

          counter = counter + 1
        }


        zip.close()
      })

      zipThread.start()

      val source = StreamConverters.fromInputStream(() => pipedIn)
      Ok.chunked(source).as("application/zip").withHeaders("Content-Disposition" -> s"attachment; filename=$rootFolderName.zip")
    } catch {
      case e: IllegalArgumentException =>
        println(s"Error decoding URLs: ${e.getMessage}")
        BadRequest(s"Problem decoding URLs: ${e.getMessage}")
      case e: Exception =>
        println(s"An unexpected error occurred 102: ${e.getMessage}")
        InternalServerError(s"An unexpected error occurred 103: ${e.getMessage}")
    }


  }

  private def appendMarcXmlFooter(content: String) = {
    content + "\n</collection>"
  }

  private def appendJsFooter(content: String, counter: Int) = {
    val jsSuffix = counter match {
      case 1 => "\n]"
      case _ => "\n])"
    }
    //we remove the last comma and the last new line character and add the corresponding suffix
    content.dropRight(2) + jsSuffix
  }

  private def getContentHeader(counter: Int) = {
    val csvFileContent = fullExportMapper.getCsvHeader
    val jsFileContent = counter match {
      case 1 => "var list = ["
      case _ => "list.push(...["
    }
    val marcXmlFileContent = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<collection xmlns=\"http://www.loc.gov/MARC21/slim\">"
    val urlCsvFileContent = "identifier_swisscollections;label;page_id;order;image_small;image_medium;image_large;fulltext_plain;fulltext_alto;transcription_markdown;transcription_html\n"
    val jsonLinesContent = ""
    (csvFileContent, jsFileContent, marcXmlFileContent, urlCsvFileContent, jsonLinesContent)
  }

  def addFileToZipStream(zipOutputStream: ZipOutputStream, fileName: String, content: String): Unit = {
    val zipEntry = new ZipEntry(fileName)
    zipOutputStream.putNextEntry(zipEntry)
    zipOutputStream.write(content.getBytes("UTF-8"))
    zipOutputStream.closeEntry()
  }

  /**
   * This adds the BOM for csv at the beginning
   * @param zipOutputStream
   * @param fileName
   * @param content
   */
  def addCsvFileToZipStream(zipOutputStream: ZipOutputStream, fileName: String, content: String): Unit = {
    val zipEntry = new ZipEntry(fileName)
    zipOutputStream.putNextEntry(zipEntry)
    // Write the BOM for UTF-8
    zipOutputStream.write(0xEF)
    zipOutputStream.write(0xBB)
    zipOutputStream.write(0xBF)

    //write the content
    zipOutputStream.write(content.getBytes("UTF-8"))
    zipOutputStream.closeEntry()
  }

  private def generateStaticFiles(solrQueryString: String, vufindUrl: String, rootFolderName: String, zipOutputStream: ZipOutputStream, numberOfResults: Long, currentTimeStamp: String): Unit = {

    implicit val queryDataWrites: Writes[QueryData] = Json.writes[QueryData]
    val queryData = QueryData(
      query_url =vufindUrl,
      number_of_results = numberOfResults,
      solr_query_string = solrQueryString,
      root_folder_name = rootFolderName,
      timestamp = currentTimeStamp,
      record_per_file = batchSize.toLong
    )

    val queryDataContent = "var query =" + Json.prettyPrint(Json.toJson(queryData))

    zipOutputStream.putNextEntry(new ZipEntry("ignore/js/query.js"))
    zipOutputStream.write(queryDataContent.getBytes("UTF-8"))
    zipOutputStream.closeEntry()


    createStaticZipFile(zipOutputStream, "START-HERE.html")
    createStaticZipFile(zipOutputStream, "README.txt")
    createStaticZipFile(zipOutputStream, "data/README.txt")
    createStaticZipFile(zipOutputStream, "data/csv/README.txt")

    createStaticZipFile(zipOutputStream, "data/images/README.txt")
    createStaticZipFile(zipOutputStream, "data/jsonl/README.txt")
    createStaticZipFile(zipOutputStream, "data/marcxml/README.txt")
    createStaticZipFile(zipOutputStream, "data/pdf/README.txt")
    createStaticZipFile(zipOutputStream, "data/text/README.txt")

    createStaticZipFile(zipOutputStream, "ignore/README.txt")
    createStaticZipFile(zipOutputStream, "ignore/css/bootstrap.min.css")
    createStaticZipFile(zipOutputStream, "ignore/css/style.css")


    createStaticZipFile(zipOutputStream, "ignore/html/choose-media-format.html")

    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-fulltext-alto.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-fulltext-plain.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-transcription-html.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-transcription-markdown.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-image-large.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-image-medium.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-image-small.html", numberOfResults)
    createZipFileWithJsIncludes(zipOutputStream, "ignore/html/url-pdf.html", numberOfResults)

    createStaticZipFile(zipOutputStream, "ignore/img/checkmark-green.svg")
    createStaticZipFile(zipOutputStream, "ignore/img/info_icon.svg")
    createStaticZipFile(zipOutputStream, "ignore/img/swisscollections_data_download_logo.png")

    createStaticZipFile(zipOutputStream, "ignore/js/js-check.js")




  }

  //send a file from the static-files folder to the zip stream
  private def createStaticZipFile(zip: ZipOutputStream, filename: String): Unit = {
    val folder = staticFilesFolder
    zip.putNextEntry(new ZipEntry(filename))
    val in = new BufferedInputStream(new FileInputStream(folder + "/" + filename))
    var b = in.read()
    while (b > -1) {
      zip.write(b)
      b = in.read()
    }
    in.close()
    zip.closeEntry()
  }


  private def createZipFileWithJsIncludes(zip: ZipOutputStream, filename: String, numberOfResults: Long): Unit = {
    val folder = staticFilesFolder
    //this is the string which in the original static file
    val stringToReplace = "        <!-- This line will be replaced by including all the js files generated -->"
    val replacementString = generateJsInclude(numberOfResults)
    val file = Source.fromFile(folder + "/" + filename)
    val fileContent = file.getLines().mkString("\n")
    val modifiedContent = fileContent.replaceFirst(stringToReplace, replacementString)
    zip.putNextEntry(new ZipEntry(filename))
    zip.write(modifiedContent.getBytes("UTF-8"))
    zip.closeEntry()
    file.close()
  }

  /**
   * Generates a string like this
   * <script src="../js/list_000001.js"></script>
   * <script src="../js/list_000002.js"></script>
   * ...
   *
   * for all the js files generates
   *
   * @return
   */
  def generateJsInclude(numberOfResults: Long): String = {
    val numberOfJsFiles = ((numberOfResults - 1) / batchSize) + 1

    {
      for (i <- 1 to numberOfJsFiles.toInt) yield {
        f"        <script src=\"../js/list_$i%06d.js\"></script>"
      }
    }.mkString("\n")
  }

  def getCurrentTimestamp(): String = {
    val currentTime: DateTime = DateTime.now().withZone(DateTimeZone.forID("Europe/Zurich"))
    currentTime.toString("yyyy-MM-dd-HH'h'mm")
  }

  /**
   * The prefix name of the export file is based on the search terms entered in the searchbox
   * @param solrQueryString
   * @return
   */
  def getRootFolderBaseName(solrQueryString: String): String = {
    val paramsList: MultiMapSolrParams = parseQueryString(solrQueryString)

    val qParam:String = paramsList.get("q", "").toString.replace("*:*", "")

    //val specialCharacterRegex = "[^a-zA-Z0-9]"
    val specialCharacterRegex ="[/\\?%*;|\"><.,;= ]"

    // Replace special characters with underscores
    qParam.replaceAll(specialCharacterRegex, "_")
  }

  def fetchSolrRecords(solrQueryString: String, cursorMark: String): QueryResponse = {
    val solrQuery = new SolrQuery()

    val paramsList: MultiMapSolrParams = parseQueryString(solrQueryString)

    val paramMap: Map[String, Array[String]] = paramsList.getMap.asScala.toMap

    //only keep relevant parameters for solr
    val filteredParams: Map[String, Array[String]] = paramMap.filter { case (k, _) => solrParametersToKeep.contains(k) }

    val solrParams = new MultiMapSolrParams(filteredParams.asJava)

    solrQuery.add(solrParams)


    solrQuery.setRows(batchSize)
    solrQuery.setSort("id", SolrQuery.ORDER.asc)
    solrQuery.setFields("id,title,institution,fullrecord,format_str_mv,format_parent_str_mv,digital_platform_str_mv,id_digital_platform_str_mv,iiif_manifest_url_str_mv,digital_reproduction_licence_str_mv,digital_reproduction_right_owner_str_mv,digital_reproduction_call_number_str_mv,digital_object_pages_details_str_mv")
    solrQuery.set("cursorMark", cursorMark)

    solrClient.query(solrCollection, solrQuery)
  }

  def transformToCsvBytes(exportRecord: ExportRecord): String = {
    val solrRecordAsCsvLine = fullExportMapper.mapToCsv(exportRecord)
    solrRecordAsCsvLine + "\n"
  }

  def transformToJsBytes(exportRecord: ExportRecord): String = {
    val solrRecordAsJsRecord = fullExportMapper.mapToJs(exportRecord)
    solrRecordAsJsRecord + ",\n"
  }

  def transformToJsonLines(exportRecord: ExportRecord): String = {
    //todo reuse the Js mapping ???
    val solrRecordAsJsRecord = fullExportMapper.mapToJs(exportRecord).replace("\n", "")
    solrRecordAsJsRecord + "\n"
  }

  def transformToMarcXmlBytes(record: VuFindSolrRecord): String = {
    val xml = record.get("fullrecord").toString.replace("<?xml version=\"1.0\"?>", "")
    s"$xml"
  }

}
