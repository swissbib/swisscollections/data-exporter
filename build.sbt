import sbt.Keys.*
import Dependencies._


ThisBuild / scalaVersion := "2.13.15"
ThisBuild / organization := "ch.swisscollections"
ThisBuild / organizationName := "swisscollections"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") Some(tag)
  else None
}

lazy val root = (project in file("."))
  //Be careful : a list of things are defined in Common.scala !
  .enablePlugins(GitVersioning, PlayService, PlayLayoutPlugin, Common)
  .settings(
    name := "data-exporter",
    //assemblyJarName in assembly := "app.jar",
    //test in assembly := {},
    //mainClass in assembly := Some("Main"),
    resolvers ++= Seq(
      "Typesafe" at "https://repo.typesafe.com/typesafe/releases/",
      "Restlet Repository" at "https://maven.restlet.talend.com",
      "Akka library repository" at "https://repo.akka.io/maven",
    ),
    libraryDependencies ++= Seq(
      guice,
      joda,
      codingwell,
      scalatestplusplay % Test,
      scalaTest % Test,
      log4jCore,
      alpakka,
      akka,
      solrj,
      solr_analysis,
      ws
    ),
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Xfatal-warnings"
    )

  )

PlayKeys.devSettings := Seq("play.server.http.port" -> "9005")

import com.typesafe.sbt.packager.MappingsHelper._
Universal / mappings ++= directory(baseDirectory.value / "resources")
