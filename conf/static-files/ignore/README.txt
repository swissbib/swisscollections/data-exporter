Inside the ignore folder, you can find HTML pages that you can open in your 
web browser. You can use them for downloading image and text files with a download manager plugin
such as DownThemAll!.

If you have just downloaded a data package from swisscollections for the first time, please start
by opening START_HERE.html (inside the root-level folder) in your browser.

If you already know how it works, you can directly open the file html/choose-media-format.html in your browser.

The css and js subfolders are only used by the HTML pages. You can ignore them.

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg