This data package is intended to be used on a laptop / desktop computer (not on a mobile device).

1st step : Unzip the folder inside the standard downloads folder of your system. Don't change the folder's name.

2nd step : Open the file START-HERE.html in your browser (Chrome, Firefox or Edge)

For more information, go to our documentation: https://ub-basel.atlassian.net/wiki/x/BIA2lg