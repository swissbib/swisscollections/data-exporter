Just after downloading your data package from swisscollections, the pdf folder is still empty. 
You can download one pdf per title with a download manager plugin such as DownThemAll!. Please start by opening START_HERE.html 
(inside the root-level folder) in your browser.

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg