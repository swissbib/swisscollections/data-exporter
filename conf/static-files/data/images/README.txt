Just after downloading your data package from swisscollections, the images folder is still empty. 
If any digitized pages (image files) are available for the titles in your result list, you can download them
with a download manager plugin such as DownThemAll!. Please start by opening START_HERE.html 
(inside the root-level folder) in your browser.

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg