The data folder contains all title metadata from your downloaded swisscollections query in various formats:

- csv
- jsonl
- marcxml

Each subfolder may contain several files - the metadata of your search results is split into several chunks. This way, the files are not too large. 
One file contains the metadata for a maximum of 200 titles.

The data folder also contains the subfolders 'images', 'text' and 'pdf'. These folders are empty until you download image, text and pdf files 
with the help of a download manager plugin such as DownThemAll!. For more information, please refer to the Best Practice documentation at 
https://ub-basel.atlassian.net/wiki/x/BIA2lg