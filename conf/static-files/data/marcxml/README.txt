The marcxml folder contains all title metadata from your downloaded swisscollections query in the MARCXML format.
For more information on MARCXML, please refer to https://www.loc.gov/standards/marcxml/.

The marcxml folder may contain several files - the metadata of your search results is split into several chunks. This way, the files are not too large. 
One file contains the metadata for a maximum of 200 titles.

The marcxml files contain detailed metadata for each title. 

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg