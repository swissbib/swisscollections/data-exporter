The csv folder contains all title metadata from your downloaded swisscollections query in csv format.
There are two subfolders:

- metadata
- media-file-links

The metadata subfolder contains detailed metadata for each title, such as 'physical_description', 'alternative_title', 'subject_person' etc.

The media-file-links subfolder contains download links for the digitized pages (images and texts) of each title (if available). 

Each subfolder may contain several files - the metadata of your search results is split into several chunks. This way, the files are not too large. 
One file contains the metadata for a maximum of 200 titles.

The common metadata element between the csv files inside the metadata and media-file-links folders is 'identifier_swisscollections'.

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg