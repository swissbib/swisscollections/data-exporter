Just after downloading your data package from swisscollections, the text folder is still empty. 
If any text pages are available for the titles in your result list, you can download them
with a download manager plugin such as DownThemAll!. Please start by opening START_HERE.html 
(inside the root-level folder) in your browser.

OCR text pages from e-rara.ch are available in
the following formats:

- ALTO (XML)
- plain text

Transcription pages from e-manuscripta.ch are available in the following formats:

- Markdown
- HTML

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg