The jsonl folder contains all title metadata from your downloaded swisscollections query in the JSON Lines format.
For more information on JSON Lines, please refer to https://jsonlines.org/.

The jsonl folder may contain several files - the metadata of your search results is split into several chunks. This way, the files are not too large. 
One file contains the metadata for a maximum of 200 titles.

The jsonl files contain detailed metadata for each title, as well as download links for the digitized pages (images and texts) of each title (if available). 

For more information, please refer to the Best Practice documentation at https://ub-basel.atlassian.net/wiki/x/BIA2lg