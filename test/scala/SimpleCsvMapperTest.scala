package ch.swisscollections

import modules.SimpleCsvMapper
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}



class SimpleCsvMapperTest extends AnyFunSuite {

  val simpleCsvMapper = new SimpleCsvMapper

  test("Test Mapper") {
    val record = new MockVuFindSolrRecord("991047952279705501.xml")
    val result = simpleCsvMapper.mapToCsv(record)
    println(result)
    result should equal(s""""Luzern, ZHB";"PostAuto-Karte ""hello"" world";"Bern, 2006";"1 Karte : beidseitig bedruckt, mehrfarbig ; 62 x 96 cm, gef. 23 x 12 cm";"";"";"Italienisch, Deutsch, Französisch, Englisch, Rätoromanisch";"";"991047952279705501";"=""991047952279705501\"\"\"""")
  }

  test("Test full csv") {
    val record = new MockVuFindSolrRecord("991170430919705501.xml")
    val result = simpleCsvMapper.mapToCsv(record)
    val descriptionLevel = simpleCsvMapper.getDescriptionLevel(record)
    println(result)
    println(descriptionLevel)
  }

  test ("Formatted field") {
    val record = new MockVuFindSolrRecord("991170512448705501.xml")
    val record2 = new MockVuFindSolrRecord("991170430919705501.xml")

    val allSubfields = simpleCsvMapper.getPhysicalDescription(record)
    val someSubfields = simpleCsvMapper.getPhysicalDescription(record2)
    val firstSubfield = simpleCsvMapper.getMedium(record)
    val oneSubfield = simpleCsvMapper.getPublicationStatement(record)
    val appended = simpleCsvMapper.getMedium(record2)

    allSubfields should equal("1 Band : Illustrationen ; 38 x 25,5 cm + 1 CD")
    someSubfields should equal("Noten ; 4'")
    firstSubfield should equal("Papier")
    oneSubfield should equal("1557-1564")
    appended should equal("Papier (Test)")


  }

}
