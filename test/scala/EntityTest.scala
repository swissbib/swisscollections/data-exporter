/*
 * data-exporter
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package scala

import modules.{Entities, Entity, MultivaluedField, SingleField}
import org.apache.solr.common.SolrDocument
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}
import play.api.libs.json.{Json, Writes}


class EntityTest extends AnyFunSuite {

  test("Entity Test") {
    implicit val singleFieldWrites = Json.writes[SingleField]
    implicit val multivaluedFieldWrites = Json.writes[MultivaluedField]
    implicit val entityWrites = Json.writes[Entity]
    implicit val entitiesWrites = Json.writes[Entities]

    /*
    implicit val singleFieldWrites: Writes[SingleField] = new Writes[SingleField] {
      def writes(singleField: SingleField) = singleField.value
    }*/

    val mvField = MultivaluedField(List("Berg, Alban", "Webern, Anton"))

    Json.toJson(mvField.values).toString() should equal("[\"Berg, Alban\",\"Webern, Anton\"]")

    val entity = Entity(
      name = Some("Berg, Alban"),
      identifier = Some(List("(IDREF)123", "(GND)456")),
      title = None,
      description = None,
      date = None,
      role = None
    )

    Json.toJson(entity).toString() should equal ("{\"name\":\"Berg, Alban\",\"identifier\":[\"(IDREF)123\",\"(GND)456\"]}")

    entity.toString should equal("Berg, Alban, ID: (IDREF)123, (GND)456")
  }

}
