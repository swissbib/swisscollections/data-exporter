/*
 * data-exporter
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package scala

import modules.{FullExportMapper, MultivaluedField}
import org.apache.solr.common.SolrDocument
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.must.Matchers.be
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}


class FullExportMapperTest extends AnyFunSuite {
  val fullExportMapper = new FullExportMapper

  test("Test Full Mapper") {
    val solrDocument = new SolrDocument()
    solrDocument.setField("format_parent_str_mv", "IO")
    solrDocument.setField("format_str_mv", Array("holzschnitt", "einblatt"))
    val record = new MockVuFindSolrRecord("991091757429705501.xml", solrDocument)
    val exportRecord = fullExportMapper.convertToExportRecord(record)
    val result = fullExportMapper.mapToCsv(exportRecord)
    println(result)

    val result2 = fullExportMapper.mapToJs(exportRecord)
    println(result2)

    val record2 = new MockVuFindSolrRecord("991066266239705501.xml")
    println(fullExportMapper.mapToJs(fullExportMapper.convertToExportRecord(record2)))
  }

  test("single subfield") {
    val record = new MockVuFindSolrRecord("991020867699705501.xml")
    val result = fullExportMapper.getStatementOfResponsibility(record)

    result.get.toString should equal ("von Prof. D. Wilhelm Wackernagel")

    val record2 = new MockVuFindSolrRecord("991091757429705501.xml")
    val result2 = fullExportMapper.getGeneralNote(record2)

    result2.get.toString should equal ("Mit Einleitungstexten von Filippo Beroaldo, Giacomo Antonio Balbo und Octavianus Mirandula###Druckermarke, Titelumrahmung, Zierleisten, Zierinitialen###Signaturen: A-Z⁸, Aa-Xx⁸")

    val record3 = new MockVuFindSolrRecord("991170430919705501.xml")
    val result3 = fullExportMapper.getPlaceOfPublication(record3)

    result3.get.toString should equal ("[S.l.]###Basel")

  }

  test ("single subfield by source") {
    val record = new MockVuFindSolrRecord("991170512448705501.xml")
    val result = fullExportMapper.getClassificationBasel(record)

    result.get.toString should equal ("2.5.2.3.1###3.5.2.3.1")

    val result2 = fullExportMapper.getFormerCallNumber(record)
    result2.get.toString should equal("Ehemalige Signatur: Standort: Stadtbibliothek St. Gallen, Handschriften. Signatur: Epistolae Tom. IV:350, Epistolae Tom. IV:350")

    val result3 = fullExportMapper.getSupport(record)
    result3.get.toString should equal("recto : Titelüberschrift aus roten Kapitalen und Uncialen gemischt; rote Kapitelzahlen.")
  }

  test("all subfields") {
    val record = new MockVuFindSolrRecord("991170512448705501.xml")
    val result = fullExportMapper.getAbstract(record)

    result.get.toString should equal("Es handelt sich um ein Abstract, mit einem Unterfeld###Es handelt sich um ein zweites Abstract")
    println(result)
  }

  test("dates") {
    val record2 = new MockVuFindSolrRecord("991170508842105501.xml")
    val date = {
      fullExportMapper.getDateNormalized(record2)
    }
    date.get.toString should equal("19520925/19521029")

    val record3 = new MockVuFindSolrRecord("991170504975405501.xml")
    val date3 = {
      fullExportMapper.getDateNormalized(record3)
    }
    date3.get.toString should equal("1949/1970")

    val record4 = new MockVuFindSolrRecord("991066266239705501.xml")
    val date4 = {
      fullExportMapper.getDateNormalized(record4)
    }
    date4.get.toString should equal ("1983")

    val record5 = new MockVuFindSolrRecord("991170522836405501.xml")
    val dateBC = {
      fullExportMapper.getDateNormalized(record5)
    }
    dateBC.get.toString should equal ("-0550/0599")

    val record6 = new MockVuFindSolrRecord("991170609528205501.xml")
    val date6 = {
      fullExportMapper.getDateNormalized(record6)
    }
    date6.get.toString should equal("1500/1599")

    val record7 = new MockVuFindSolrRecord("991170499444605501.xml")
    val date7 = {
      fullExportMapper.getDateNormalized(record7)
    }
    date7.get.toString should equal("1866-04")

    val record8 = new MockVuFindSolrRecord("991170514773105501.xml")
    val date8 = {
      fullExportMapper.getDateNormalized(record8)
    }
    date8.get.toString should equal("19uu")

    val record10 = new MockVuFindSolrRecord("991170461257105501.xml")
    val date10 = {
      fullExportMapper.getDateNormalized(record10)
    }
    date10.get.toString should equal("1875-06/18800315")
  }

  test("analytica") {
    val record = new MockVuFindSolrRecord("991171580104505501.xml")
    val includedIn = fullExportMapper.getIncludedIn(record)

    includedIn.get.toString should equal("Basler Stadtbuch (SLSP ID: 991123454789705501). 116(1995), S. 102-104")
  }

  test("accession") {
    val record = new MockVuFindSolrRecord("991170499444605501.xml")
    val result = fullExportMapper.getAccession(record)

    result.get.toString should equal("Geschenk. Herkunft: Ernst Rentsch (Architekt, 1876- ). Datum: 1932. Akzessionsnummer: *32,2513. Eigentümer: Öffentliche Bibliothek der Universität Basel")

  }

  test("language") {
    val record = new MockVuFindSolrRecord("991004584039705501.xml")
    val result: Option[MultivaluedField] = fullExportMapper.getLanguage(record)

    result.get.toString should equal("Englisch###Lateinisch###Gälisch###Deutsch")

  }


  test("entities") {
    val record = new MockVuFindSolrRecord("991091757429705501.xml")
    val persons = fullExportMapper.getAuthorPerson(record)

    persons.get.toString should equal("Balbo, Giacomo Antonio, ID: (DE-588)1087860970###Beroaldo, Filippo, ID: (DE-588)118656589###Mirandula, Octavianus###Hagenbach, Jacob, ID: (DE-588)1089800193, Role: VorbesitzerIn###Hagenbach, Jacob II, ID: (DE-588)1089800193, Role: VorbesitzerIn, Exemplar der ZB Zürich")

    val corporates = fullExportMapper.getAuthorCorporate(record)

    corporates.get.toString should equal("Nicolaus Brylinger Erben, ID: (DE-588)6145992-6, Role: DruckerIn")

    val record2 = new MockVuFindSolrRecord("991003771979705501.xml")
    val works = fullExportMapper.getWork(record2)

    works.get.toString should equal("Ryhiner-Kartensammlung, 7203###Pläne und Ansichten. China###Alpen-Sinfonie, F-moll, Fagott ; arrangiert / Berg, Alban (1885-1935), ID: (DE-588)118509322###Test-Sinfonie / Berg, Alban (1885-1935)")

    val record3 = new MockVuFindSolrRecord("991073834589705501.xml")
    val personsWithIdref = fullExportMapper.getAuthorPerson(record3)

    personsWithIdref.get.toString should equal("Bach, Johann Sebastian, ID: (IDREF)026699656, (DE-588)11850553X, Role: KomponistIn###Bret, Gustave, ID: (IDREF)083565337, (DE-588)1020868511, Role: ÜbersetzerIn")

  }

  test("solr fields") {
    val solrDocument = new SolrDocument()
    solrDocument.setField("format_parent_str_mv", "IO")
    solrDocument.setField("format_str_mv", Array("holzschnitt", "einblatt"))

    val record = new MockVuFindSolrRecord("991091757429705501.xml", solrDocument)
    val parentResourceType = fullExportMapper.getParentResourceType(record)
    val resourceType = fullExportMapper.getResourceType(record)

    parentResourceType.get.toString should equal("IO")
    resourceType.get.toString should equal("Holzschnitt###Einblattdruck")
  }


  test("form") {
    val record = new MockVuFindSolrRecord("991066266239705501.xml")
    val forms = fullExportMapper.getForm(record)


    forms.get.toString should equal("Bild###Fotografie###Dia###Ansicht [Formschlagwort Sondersammlungen]###Stimme")

    val subjectMusic = fullExportMapper.getSubjectMusic(record)
    subjectMusic.get.toString should equal("Faksimile, 17.-18. Jh.")

    val instrumentation = fullExportMapper.getInstrumentation(record)
    instrumentation.get.toString should equal("Singstimme (mehrere), Chor (1), Orchester (1), Bass###Sopran (mehrere), Tenor (mehrere)###Violine (3), Cello, Schlagzeug (1)")

    val zurichClassification = fullExportMapper.getClassificationZurich(record)
    zurichClassification.get.toString should equal("ZueBiSACH01_Z01###ZueBiSACH02_Z01")

    val thumbnail = fullExportMapper.getThumbnail(record)
    thumbnail.get.toString should equal("https://www.e-rara.ch/titlepage/doi/10.3931/e-rara-69464/128")
  }


  test("contents") {
    val record = new MockVuFindSolrRecord("991018009989705501.xml")
    val contents = fullExportMapper.getContents(record)

    contents.getOrElse("").toString should equal("1, Organum###2, Physicorum libri VIII###3, Metaphysicorum libri XIIII, Theophrasti Metaphysicorum liber###4, Libri omnes, quibus historia partes animalium, atque plantarum pertractantur, Paracelsus###Libri omnes, quibus tota moralis philosophia continetur###6###7, Hypokras")
  }


  test("publication statement"){
    val record = new MockVuFindSolrRecord("991066266239705501.xml")
    val value = fullExportMapper.getPublicationStatementFull(record)
    value.get.toString should equal("[Schweiz] : [Kodak], [1983]")
  }

  test ("citation") {

    val solrDocument = new SolrDocument()
    solrDocument.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument.setField("digital_reproduction_right_owner_str_mv", "Zentralbibliothek Zürich")
    solrDocument.setField("digital_reproduction_call_number_str_mv", "Gal Sp 181: d")
    val record = new MockVuFindSolrRecord("991004584039705501.xml", solrDocument)
    val citation = fullExportMapper.getReproductionCitation(record)
    citation.get.toString should equal("Lavater, Johann Caspar: J.C. Lavaters physiognomische Fragmente zur Beförderung der Menschenkenntniss und Menschenliebe. Winterthur : in Verlag Heinrich Steiners und Compagnie, 1783-1830. Zentralbibliothek Zürich, Gal Sp 181: d https://doi.org/10.3931/e-rara-18232 / Public Domain Mark")

    val solrDocument2 = new SolrDocument()
    solrDocument2.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument2.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Bern")
    solrDocument2.setField("digital_reproduction_call_number_str_mv", "MUE Ryh 7203")
    val record2 = new MockVuFindSolrRecord("991003771979705501.xml", solrDocument2)
    val citation2 = fullExportMapper.getReproductionCitation(record2)
    citation2.get.toString should equal("Imperii Sinarum provinciae exteriores : Korea, Pläne und Prospekte : Sammelband der Ryhiner-Kartensammlung. [Verschiedene Orte], ca. 1660-1833. Universitätsbibliothek Bern, MUE Ryh 7203 https://doi.org/10.3931/e-rara-96586 / Public Domain Mark")

    val solrDocument3 = new SolrDocument()
    solrDocument3.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument3.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Basel")
    solrDocument3.setField("digital_reproduction_call_number_str_mv", "Portr Falk:1-518")
    val record3 = new MockVuFindSolrRecord("991170456639105501.xml", solrDocument3)
    val citation3 = fullExportMapper.getReproductionCitation(record3)
    citation3.get.toString should equal("Sarasin, Jakob: Sammlung von Bildnissen zur Basler Geschichte gehörend. Basel, 1775-1856. Universitätsbibliothek Basel, Portr Falk:1-518 https://doi.org/10.7891/e-manuscripta-19795 / Public Domain Mark")

    val solrDocument4 = new SolrDocument()
    solrDocument4.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument4.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Basel")
    solrDocument4.setField("digital_reproduction_call_number_str_mv", "UBH DB IX 14")
    val record4 = new MockVuFindSolrRecord("991091757429705501.xml", solrDocument4)
    val citation4 = fullExportMapper.getReproductionCitation(record4)
    citation4.get.toString should equal("Illustrium poetarum flores. Basileae : Typis Brylingerianis, anno 1599. Universitätsbibliothek Basel, UBH DB IX 14 https://doi.org/10.3931/e-rara-561 / Public Domain Mark")

    val solrDocument5 = new SolrDocument()
    solrDocument5.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument5.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Basel")
    solrDocument5.setField("digital_reproduction_call_number_str_mv", "An I 20:7")
    val record5 = new MockVuFindSolrRecord("991020867699705501.xml", solrDocument5)
    val citation5 = fullExportMapper.getReproductionCitation(record5)
    citation5.get.toString should equal("Wackernagel, Wilhelm: Über die mittelalterliche Sammlung zu Basel : nebst einigen Schriftstücken aus derselben. Basel : Schweighauserische Universitaets-Buchdruckerei, 1857. Universitätsbibliothek Basel, An I 20:7 https://doi.org/10.3931/e-rara-79772 / Public Domain Mark")

    val solrDocument6 = new SolrDocument()
    solrDocument6.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument6.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Basel")
    solrDocument6.setField("digital_reproduction_call_number_str_mv", "An xxx")
    val record6 = new MockVuFindSolrRecord("991170508842105501.xml", solrDocument6)
    val citation6 = fullExportMapper.getReproductionCitation(record6)
    citation6.get.toString should equal("Sabais, Heinz Winfried ; Burckhardt, Carl Jacob: Korrespondenz zwischen Heinz Winfried Sabais und Carl Jacob Burckhardt. Darmstadt, Zürich. Universitätsbibliothek Basel, An xxx / Public Domain Mark")

    val solrDocument7 = new SolrDocument()
    solrDocument7.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument7.setField("digital_reproduction_right_owner_str_mv", "Universitätsbibliothek Basel")
    solrDocument7.setField("digital_reproduction_call_number_str_mv", "UBH AN VI 26c")
    val record7 = new MockVuFindSolrRecord("991170733998805501.xml", solrDocument7)
    val citation7 = fullExportMapper.getReproductionCitation(record7)
    citation7.get.toString should equal ("Wilhelm von Nassau ; Jollivet ; Colas, Samuel ; Dietz, Johann Heinrich ; Schütz, Johann Andreas ; Johann Christian Andreas ; Friedrich Magnus ; Esselbrunn, Johann Franziskus ; Stolle, Thomas ; Nassau-Dillenburg, Adolf von ; Dilthey, Johann Eberwin ; Tormann, Ulrich ; Löwen, Johann von ; Reber, Johann Heinrich ; Ringle, Johann Jacob ; Beyer, Arnold Gerhard de ; Marquart, Otto Christoph ; Diltey, Philipp ; Lent, Johann von ; Brandt, Theodor Everhard ; Rosenburger, Peter ; Curike, Johann ; Faber, Johann Jacob ; Willadinus, Sigismund ; Steinwehr, Johann Friedrich von ; Bremer, Johann ; Holzklau, Wilhelm ; Liepoldt, Johann Nicolaus ; Henric-Petri, Jacob ; Marrel, Jacob ; Graff, Johann Andreas ; Vultejus, Justus Hermann ; Ammann, Christoph Sigmund ; Utzinger, Hans Friedrich ; Merer, Marcus Christoph ; Schorsch, Johann Georg ; Bachofen, Johann Ulrich ; Gerbrandt, Paul ; Aantzaunus, Hans ; Line, Albert von ; Reichmann, Gottfried ; Hildebrand, Hermann ; Seyler, Friedrich ; Ramspeck, Sebastian ; Scandolera, Johann ; Crollius, Johann Lorenz ; Nethenus, Matthias ; Winckler, Anton ; Beck, Jakob ; Schindler, Johann Heinrich ; Eglinger, Nicolaus ; Faverger, Peter von ; Heschler, Johann Adam ; Wallebry, Harley ; Hofmann, J.L. ; Du Gue, Jakob ; Socin, Joseph ; Schönau, Hans von ; Müller, G.G. ; Hoppe, Hermann ; Scriba, Johann Jacob ; Faesch, Remigius ; Dury, John ; Gualterio, Petrus ; Reidanus, Florens ; Beyer, Johan de ; Buxtorf, Johann ; Rhode, Claus ; Rhode, Heinrich ; Schindel, Georg Rudolf von ; Tschirnhaus, Ehrenfried Walther von ; Erlach, R. von ; Home, J. ; Werenfels, Rudolf ; Enopius, Ludwig Christian ; Roht, Christian ; Häsbärt, Martin Johann ; Zwinger, Johannes ; Burckhard, Johann Friedrich ; Burckard, Johann Rudolph ; Gantesweiler, Johann Jacob ; Solms-Braunfels, Wilhelm Moritz von ; Du Moulin, Marie ; Johann Reinhard ; Holtermann, Arnold Moritz ; Lombard, André ; Georg Ludovicus ; Rousselet, Susanne ; Le-Clerc ; Benzon Danois ; Asperling, B. d' ; Launay, Henry de ; Brandmüller, Jakob ; Sacrelairer, Heinrich ; Dattinus, Simon ; Junell, Petrus ; Darselliers, Kaspar ; Dulloux, Paul ; Gaudardus, Ludwig ; Jakob von Alpen ; Rondellus, Jakob ; Burckhardt, Philipp ; Czernin von Chudenitz, Jeremias ; Heeser, Johann ; Stockarus, Eberhard ; Hanus, Josef: Stammbuch. Verschiedene Orte, 1660-1690. Universitätsbibliothek Basel, UBH AN VI 26c https://doi.org/10.7891/e-manuscripta-121083 / Public Domain Mark")

    /*val solrDocument8 = new SolrDocument()
    solrDocument8.setField("digital_reproduction_licence_str_mv", "pdm")
    solrDocument8.setField("digital_platform_str_mv", "e-rara.ch")
    solrDocument8.setField("digital_reproduction_right_owner_str_mv", "Zentralbibliothek Zürich")
    solrDocument8.setField("digital_reproduction_call_number_str_mv", "NNN 829")
    val record8 = new MockVuFindSolrRecord("991114397489705501.xml", solrDocument8)
    val citation8 = fullExportMapper.getReproductionCitation(record8)
    citation8.get.toString should equal("Vallisnieri, Antonio: Opere diverse : 1. Istoria del camaleonte affricano e di vari animali d'Italia / del sig. Antonio Vallisnieri. In Venezia. In Venezia : apresso Gio. Gabbriello Ertz, 1715. Zentralbibliothek Zürich. , NNN 829 https://doi.org/10.3931/e-rara-25102 / Public Domain Mark")*/
  }

  test ("ismn") {
    val record = new MockVuFindSolrRecord("991004584039705501.xml")
    val ismn = fullExportMapper.getIsmn(record)
    ismn.get.toString should equal("M007086367")
  }

  test ("original script") {
    val record = new MockVuFindSolrRecord("991170507401805501.xml")
    val title = fullExportMapper.getTitleScript(record)
    title.get.toString should equal("لب اللب وسر السر")

    val place = fullExportMapper.getPlaceOfPublicationScript(record)
    place.get.toString should equal("Женева")

    val publisher = fullExportMapper.getPublisherScript(record)
    publisher.get.toString should equal("Типография Союза русских социалдемократов")

    val contents = fullExportMapper.getContentsScript(record)
    contents.get.toString should equal("Женева, социалдемократов###لب اللب وسر السر, الحمد لله الرزاق و رب العالمين ... معلوم اوله كه بو رساله شريفه يى اجلة رجال طرق ... وعلماء اهل كمال وتاليفه بى مثال زكيه دن اسماعيل حقى قدس سره السامى ورحمة الله عليه البارى ... ‒ ... بس ايكى عالم بعض [؟] مكوناب وآدم آينه س[..]در كور بنام بر يوز در غيرى دكلدر والسلام خاتمت الكلام بلطفه ملك العلّام و بكرم سيد الانام عليه افضل الصلوة والسلام فى غرة محرم الحرام سنه ١٢٩٧ ..., اسماعيل حقّى")

  }

  test ("page download links") {
    val solrDocument = new SolrDocument()
    solrDocument.setField("digital_platform_str_mv", "e-rara.ch")
    solrDocument.setField("digital_object_pages_details_str_mv", Array(
      "2940906###Scan 2###1###0",
      "2940907###Scan 3###2###0",
      "2940908###Scan 4###3###0",
      "2940909###Scan 5###4###0",
      "2940910###Scan 6###5###0",
      "2940911###Scan 7###6###1",
      "2940912###Scan 8###7###0",
      "2940913###Scan 9###8###0"))
    solrDocument.setField("id_digital_platform_str_mv", "2940903")
    val record = new MockVuFindSolrRecord("991170508842105501.xml", solrDocument)
    val exportRecord = fullExportMapper.convertToExportRecord(record)


    val pages = fullExportMapper.getPageDownloadLinks(record).get

    assert(pages.pageDownloadLinks.head.label.get == "Scan 2")
    assert(pages.pageDownloadLinks.head.image_medium.get == "https://www.e-rara.ch/download/webcache/504/2940906")
    pages.pageDownloadLinks.head.fulltext_alto should be (None)
    val res = pages.pageDownloadLinks.toList
    assert(res(5).fulltext_plain.get == "https://www.e-rara.ch/download/fulltext/plain/2940911")

    val res2 = fullExportMapper.generateUrlCsv(exportRecord)
    val expected = "991170508842105501;\"Scan 2\";2940906;1;https://www.e-rara.ch/download/webcache/128/2940906;https://www.e-rara.ch/download/webcache/504/2940906;https://www.e-rara.ch/download/webcache/0/2940906;;;;\n991170508842105501;\"Scan 3\";2940907;2;https://www.e-rara.ch/download/webcache/128/2940907;https://www.e-rara.ch/download/webcache/504/2940907;https://www.e-rara.ch/download/webcache/0/2940907;;;;\n991170508842105501;\"Scan 4\";2940908;3;https://www.e-rara.ch/download/webcache/128/2940908;https://www.e-rara.ch/download/webcache/504/2940908;https://www.e-rara.ch/download/webcache/0/2940908;;;;\n991170508842105501;\"Scan 5\";2940909;4;https://www.e-rara.ch/download/webcache/128/2940909;https://www.e-rara.ch/download/webcache/504/2940909;https://www.e-rara.ch/download/webcache/0/2940909;;;;\n991170508842105501;\"Scan 6\";2940910;5;https://www.e-rara.ch/download/webcache/128/2940910;https://www.e-rara.ch/download/webcache/504/2940910;https://www.e-rara.ch/download/webcache/0/2940910;;;;\n991170508842105501;\"Scan 7\";2940911;6;https://www.e-rara.ch/download/webcache/128/2940911;https://www.e-rara.ch/download/webcache/504/2940911;https://www.e-rara.ch/download/webcache/0/2940911;https://www.e-rara.ch/download/fulltext/plain/2940911;https://www.e-rara.ch/download/fulltext/alto3/2940911;;\n991170508842105501;\"Scan 8\";2940912;7;https://www.e-rara.ch/download/webcache/128/2940912;https://www.e-rara.ch/download/webcache/504/2940912;https://www.e-rara.ch/download/webcache/0/2940912;;;;\n991170508842105501;\"Scan 9\";2940913;8;https://www.e-rara.ch/download/webcache/128/2940913;https://www.e-rara.ch/download/webcache/504/2940913;https://www.e-rara.ch/download/webcache/0/2940913;;;;\n"
    assert (res2 == expected)

  }

  test ("uniform title") {
    val record = new MockVuFindSolrRecord("991004584039705501.xml")
    val res = fullExportMapper.getUniformTitle(record)
    assert (res.get.toString == "Physiognomische Fragmente")

    val record2 = new MockVuFindSolrRecord("991003771979705501.xml")
    val res2 = fullExportMapper.getUniformTitle(record2)

    res2 should be (None)
  }

  test ("item call numbers") {
    val record = new MockVuFindSolrRecord("991047952279705501.xml")
    val res = fullExportMapper.getAddCallNumber(record)
    assert (res.get.toString == "Luzern, ZHB, KA 269 (1)")
  }

  test ("empty lists") {
    val record = new MockVuFindSolrRecord("991121441279705501.xml")
    val exportRecord = fullExportMapper.convertToExportRecord(record)
    val result = fullExportMapper.mapToJs(exportRecord)

    val expected = "{\n  \"library_call_number\" : [\"Basel, Universitätsbibliothek, UBH\"],\n  \"digital_reproduction\" : [\"http://www.ub.unibas.ch/digi/a100/portraet/bs/m/IBB_1_004910393.jpg\"],\n  \"title\" : \"Bernhard Meier\",\n  \"statement_of_responsibility\" : \"[Hans Hug Kluber]\",\n  \"date_of_publication_or_production\" : [\"[1513]\"],\n  \"date_of_publication_or_production_standardised\" : \"1513\",\n  \"physical_description\" : [\"1 Reproduktion eines Gemäldes : schwarz/weiss ; 16,7 x 14 cm\"],\n  \"person\" : [{\"name\":\"Kluber, Hans Hug\"}],\n  \"subject_person\" : [{\"name\":\"Meyer, Bernhard\",\"date\":\"1488-1558\",\"identifier\":[\"(DE-588)138373159\"]},{\"name\":\"Meyer, Bernhard\",\"date\":\"1488-1558\"},{\"name\":\"Meyer, Bernhard\",\"date\":\"1488-1558\"}],\n  \"subject_form\" : [\"Bildnis\"],\n  \"general_note\" : [\"Original: Tempera auf Holz, Kunstmuseum Basel (Copyright)\",\"Bürgermeister von Basel\",\"Genannt Bernhard Meyer zum Pfeil\"],\n  \"additional_call_numbers\" : [\"Basel, Universitätsbibliothek, UBH Portr BS Meyer B 1488, 1b\",\"Basel, Universitätsbibliothek, UBH Portr BS Meyer B 1488, 1c\",\"Basel, Universitätsbibliothek, UBH Portr BS Meyer B 1488, 1a\"],\n  \"identifier_swisscollections\" : \"991121441279705501\",\n  \"different_system_control_number\" : [\"(swissbib)057979812-41slsp_network\",\"057979812\",\"(IDSBB)004910393DSV01\",\"(EXLNZ-41SLSP_NETWORK)991121441279705501\",\"(41SLSP_UBS)9949103930105504\"]\n}"

    assert (result == expected)

  }


}
