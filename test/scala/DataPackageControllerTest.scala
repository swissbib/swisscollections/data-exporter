/*
 * data-exporter
 * Copyright (C) 2023  UB Basel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package scala

import controllers.DataPackageController
import org.scalatest.Tag
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers.{convertToAnyShouldWrapper, equal}
import play.api.libs.json.Json

object NeedsSolrConnection extends Tag("ch.swisscollections.tags.NeedsSolrConnection")

class DataPackageControllerTest extends AnyFunSuite {


  /*

  test("Get Number of Results Test", NeedsSolrConnection) {
    val dpc = new DataPackageController
    val numberOfResults = dpc.getNumberOfResults("id:991041346059705501")
    assert(numberOfResults == 1)
  }

  test("Get Js include") {
    val dpc = new DataPackageController
    val res = dpc.generateJsInclude(300)
    val expected = List("        <script src=\"../js/list_000001.js\"></script>", "        <script src=\"../js/list_000002.js\"></script>").mkString("\n")
    assert (res == expected)
  }

   */
}
