# Data Exporter

The Goal of this microsservice is to extract swisscollections data in various formats : csv, json, xml

It is an API based on Scala Play Framework. 

cf. https://ub-basel.atlassian.net/wiki/spaces/RH/pages/2144436230/swisscollections+Data

# run it

you can use scala play run config in intellij

or sbt run (needs an access to solrtest.swisscollections.ch to work, you need to be in VPN)
then it is available on localhost:9000


big 50'000 results
localhost:9000/data-package?solr_query=fq%3DpublishDate%3A[1900 TO *]%26q%3Dlanguage%3Afre&vufind_url=test

small 
localhost:9005/data-package?solr_query=fq%3DpublishDate%3A[1801 TO 1802]%26q%3Dlanguage%3Adan&vufind_url=test
localhost:9005/csv-export?solr_query=fq%3DpublishDate%3A[1801 TO 1802]%26q%3Dlanguage%3Adan

middle
localhost:9005/data-package?solr_query=fq%3DpublishDate%3A[1900 TO 1910]%26q%3Dlanguage%3Afre&vufind_url=test
localhost:9005/csv-export?solr_query=fq%3DpublishDate%3A[1900 TO 1910]%26q%3Dlanguage%3Afre

without accents : 
localhost:9005/data-package?vufind_url=test%26&solr_query=q%3D%22algebre%20lineaire%22%26df%3Dtopic

with accents : 
localhost:9005/data-package?vufind_url=https%3A%2F%2Ftest.swisscollections.ch%2FSearch%2FResults%3Fsort%3Drelevance%26join%3DAND%26bool0%255B%255D%3DAND%26lookfor0%255B%255D%3Dalg%25C3%25A8bre%26type0%255B%255D%3DSubject%26lookfor0%255B%255D%3D%26type0%255B%255D%3DAllFields%26lookfor0%255B%255D%3D%26type0%255B%255D%3DAllFields%26daterange%255B%255D%3DpublishDate%26publishDatefrom%3D%26publishDateto%3D%26limit%3D20%26&solr_query=q%3D%2522alg%25C3%25A8bre%2522%26df%3Dtopic


localhost:9005/data-package?solr_query=q%3Did%3A991170603846505501%2BOR%2Bid%3A991003771979705501%2BOR%2Bid%3A991084004469705501&vufind_url=test

localhost:9005/data-package?solr_query=q%3Did%3A991170603846505501&vufind_url=test
localhost:9005/csv-export?solr_query=q%3Did%3A991170603846505501

On prod : 
https://data-exporter-prod-swisscollections.k8s-001.unibas.ch/data-package?solr_query=fq%3DpublishDate%3A[1801 TO 1802]%26q%3Dlanguage%3Adan&vufind_url=test




# run tests

sbt test





