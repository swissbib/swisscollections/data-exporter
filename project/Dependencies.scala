/*
 * OAI Server Interface
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import play.sbt.PlayImport.ws
import sbt._

object Dependencies {

  lazy val scalatestV = "3.1.2"
  lazy val log4jV = "2.17.1"
  lazy val log4jscalaV  = "12.0"
  //lazy val apachePoiV = "5.2.2" //compatible with log4j 2.17.1


  lazy val joda = "org.joda" % "joda-convert" % "2.2.1"

  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV

  lazy val codingwell = "net.codingwell" %% "scala-guice" % "4.2.6"

  lazy val scalatestplusplay = "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0"

  lazy val alpakka = "com.lightbend.akka" %% "akka-stream-alpakka-file" % "4.0.0"

  lazy val akka = "com.typesafe.akka" %% "akka-stream" % "2.6.20"

  lazy val scala_xml_module = "org.scala-lang.modules" %% "scala-xml" % "2.3.0"

  lazy val solrj = "org.apache.solr" % "solr-solrj" % "7.3.1"

  lazy val solr_analysis = "org.apache.solr" % "solr-analysis-extras" % "7.3.1"

  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalatestV

  lazy val wsDep = ws
}
